import React from "react";
import { makeStyles} from '@material-ui/core/styles';
import './Layout.css';
import SideBar from '../../containers/SideBar/sidebar'

const useStyles = makeStyles((theme) => ({
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

export default function Layout(props) {
    const classes = useStyles();

    return (  
        <div>
            <SideBar/>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {props.children}
            </main>
        </div>
    );
}
 
