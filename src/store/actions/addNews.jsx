import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const addNewsStart = () => {
    return {
        type: actionTypes.ADD_NEWS_START
    };
};

export const addNewsSuccess = (data) => {
    return {
        type: actionTypes.ADD_NEWS_SUCCESS,
        data: data,
        isSuccess: data.isSuccess
    }
};

export const addNewsFail = (error) => {
    return {
        type: actionTypes.ADD_NEWS_FAIL,
        error: error
    };
};

export const closeModalErrorAddNews = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_ADD_NEWS
    };
};

export const addNews =(news)=> {
    return dispatch => {
        dispatch( addNewsStart());
    
        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.post(`/Homepage/news`,news,config)
        .then(response => {
            dispatch(addNewsSuccess(response.data));
            console.log(response);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(addNewsFail(error.response));
            }
        });
    };
};