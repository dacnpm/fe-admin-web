import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getTestingStart = () => {
    return {
        type: actionTypes.GET_TESTING_START
    };
};

export const getTestingSuccess = (data, isSuccess) => {
    return {
        type: actionTypes.GET_TESTING_SUCCESS,
        data: data,
        isSuccess: isSuccess
    }
};

export const getTestingFail = (error) => {
    return {
        type: actionTypes.GET_TESTING_FAIL,
        error: error
    };
};

export const closeModalErrorGetTesting = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_TESTING
    };
};

export const getTesting =()=> {
    return dispatch => {
        dispatch(getTestingStart());

        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/testing?PageSize=${100}`, config)
        .then(response => {
            dispatch(getTestingSuccess(response.data.data, response.data.isSuccess));
            //console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getTestingFail(error.response.data.message));
            }
        });
    };
};