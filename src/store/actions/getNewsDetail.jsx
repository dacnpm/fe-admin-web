import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getNewsDetailStart = () => {
    return {
        type: actionTypes.GET_NEWS_DETAIL_START
    };
};

export const getNewsDetailSuccess = (data, isSuccess) => {
    return {
        type: actionTypes.GET_NEWS_DETAIL_SUCCESS,
        data: data,
        isSuccess: isSuccess
    }
};

export const getNewsDetailFail = (error) => {
    return {
        type: actionTypes.GET_NEWS_DETAIL_FAIL,
        error: error
    };
};

export const closeModalErrorgetNewsDetail = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_NEWS_DETAIL
    };
};

export const getNewsDetail =(postId)=> {
    return dispatch => {
        dispatch(getNewsDetailStart());
    
        const config = {
          headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Homepage/news/${postId}`, config)
        .then(response => {
            dispatch(getNewsDetailSuccess(response.data.data, response.data.isSuccess));
            //console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getNewsDetailFail(error.response.data.message));
            }
        });
    };
};