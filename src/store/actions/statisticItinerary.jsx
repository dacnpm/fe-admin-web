import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const statisticItineraryStart = () => {
    return {
        type: actionTypes.STATISTIC_ITINERARY_START
    };
};

export const statisticItinerarySuccess = (data, isSuccess) => {
    return {
        type: actionTypes.STATISTIC_ITINERARY_SUCCESS,
        data: data,
        isSuccess: isSuccess
    }
};

export const statisticItineraryFail = (error) => {
    return {
        type: actionTypes.STATISTIC_ITINERARY_FAIL,
        error: error
    };
};

export const closeModalErrorStatisticItinerary = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_STATISTIC_ITINERARY
    };
};

export const statisticItinerary =(startDate, endDate)=> {
    return dispatch => {
        dispatch(statisticItineraryStart());

        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/itinerary/statistic?startDate=${startDate}&endDate=${endDate}`, config)
        .then(response => {
            dispatch(statisticItinerarySuccess(response.data.data, response.data.isSuccess));
            console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(statisticItineraryFail(error.response.data.message));
            }
        });
    };
};