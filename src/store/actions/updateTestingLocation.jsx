import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const updateLocationStart = () => {
    return {
        type: actionTypes.UPDATE_LOCATION_START
    };
};

export const updateLocationSuccess = (data) => {
    return {
        type: actionTypes.UPDATE_LOCATION_SUCCESS,
        data: data,
        isSuccess: data.isSuccess
    }
};

export const updateLocationFail = (error) => {
    return {
        type: actionTypes.UPDATE_LOCATION_FAIL,
        error: error
    };
};

export const closeModalErrorUpdateLocation = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_UPDATE_LOCATION
    };
};

export const updateLocation =(location)=> {
    return dispatch => {
        dispatch(updateLocationStart());
        
        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.put(`testing/location/${location.id}`,location,config)
        .then(response => {
            dispatch(updateLocationSuccess(response.data));
            console.log(response);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(updateLocationFail(error.response));
            }
        });
    }
};