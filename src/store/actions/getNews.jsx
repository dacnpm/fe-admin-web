import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getNewsStart = () => {
    return {
        type: actionTypes.GET_NEWS_START
    };
};

export const getNewsSuccess = (data, isSuccess) => {
    return {
        type: actionTypes.GET_NEWS_SUCCESS,
        data: data,
        isSuccess: isSuccess
    }
};

export const getNewsFail = (error) => {
    return {
        type: actionTypes.GET_NEWS_FAIL,
        error: error
    };
};

export const closeModalErrorGetNews = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_NEWS
    };
};

export const getNews =()=> {
    return dispatch => {
        dispatch(getNewsStart());

        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Homepage/allnews`, config)
        .then(response => {
            dispatch(getNewsSuccess(response.data.data, response.data.isSuccess));
            //console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getNewsFail(error.response.data.message));
            }
        });
    };
};