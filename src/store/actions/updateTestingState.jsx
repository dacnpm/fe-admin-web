import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const updateTestingStart = () => {
    return {
        type: actionTypes.UPDATE_TESTING_START
    };
};

export const updateTestingSuccess = (data) => {
    return {
        type: actionTypes.UPDATE_TESTING_SUCCESS,
        data: data
    }
};

export const updateTestingFail = (error) => {
    return {
        type: actionTypes.UPDATE_TESTING_FAIL,
        error: error
    };
};

export const closeModalErrorUpdateTesting = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_UPDATE_TESTING
    };
};

export const updateTesting =(testingId, updatedData)=> {
    updateTestingStart();
    const testing = {
        testingState: updatedData.testingState,
        result: updatedData.result,
        isPaid: updatedData.isPaid,
    }

    const config = {
        headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
    };
    axios.put(`/Admin/testing/${testingId}`,testing,config)
    .then(response => {
        updateTestingSuccess(response);
        console.log(response);
    })
    .catch(error => {
        if(error.response === undefined){
            setTimeout(() => {
                window.location = "/network-error";
            }, 1500)
        }else{
            updateTestingFail(error.response);
        }
    });
};