import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getUsersStart = () => {
    return {
        type: actionTypes.GET_USERS_START
    };
};

export const getUsersSuccess = (data, isSuccess) => {
    return {
        type: actionTypes.GET_USERS_SUCCESS,
        data: data,
        isSuccess: isSuccess
    }
};

export const getUsersFail = (error) => {
    return {
        type: actionTypes.GET_USERS_FAIL,
        error: error
    };
};

export const closeModalErrorGetUsers = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_USERS
    };
};

export const getUsers =()=> {
    return dispatch => {
        dispatch(getUsersStart());

        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/user-statistic?PageSize=${1000}`, config)
        .then(response => {
            dispatch(getUsersSuccess(response.data.data, response.data.isSuccess));
            //console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getUsersFail(error.response.data.message));
            }
        });
    };
};