import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const deleteLocationStart = () => {
    return {
        type: actionTypes.DELETE_LOCATION_START
    };
};

export const deleteLocationSuccess = (data) => {
    return {
        type: actionTypes.DELETE_LOCATION_SUCCESS,
        data: data
    }
};

export const deleteLocationFail = (error) => {
    return {
        type: actionTypes.DELETE_LOCATION_FAIL,
        error: error
    };
};

export const closeModalErrorDeleteLocation = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_DELETE_LOCATION
    };
};

export const deleteLocation =(locationId)=> {
    deleteLocationStart();
    
    const config = {
      headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
    };
    axios.delete(`testing/location/${locationId}`,config)
    .then(response => {
        deleteLocationSuccess(response);
        console.log(response);
    })
    .catch(error => {
        if(error.response === undefined){
            setTimeout(() => {
                window.location = "/network-error";
            }, 1500)
        }else{
            deleteLocationFail(error.response);
        }
    });
};