import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const uploadImageStart = () => {
    return {
        type: actionTypes.UPLOAD_IMAGE_START
    };
};

export const uploadImageSuccess = (data) => {
    return {
        type: actionTypes.UPLOAD_IMAGE_SUCCESS,
        data: data,
        isSuccess: data.isSuccess
    }
};

export const uploadImageFail = (error) => {
    return {
        type: actionTypes.UPLOAD_IMAGE_FAIL,
        error: error
    };
};

export const closeModalErrorUploadImage = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_UPLOAD_IMAGE
    };
};

export const uploadImage =(newsId, formData)=> {
    console.log(formData.get("newsImg"))
    return dispatch => {
        dispatch(uploadImageStart());
        
        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}`,
        'content-type': 'application/x-www-url-formencoded',
        }
        };
        axios.post(`/Homepage/news/image/${newsId}`,formData,config)
        .then(response => {
            dispatch(uploadImageSuccess(response.data));
            //console.log(response.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(uploadImageFail(error.response));
            }
        });
    };
};