import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getUserTestingStart = () => {
    return {
        type: actionTypes.GET_USER_TESTING_START
    };
};

export const getUserTestingSuccess = (data) => {
    return {
        type: actionTypes.GET_USER_TESTING_SUCCESS,
        data: data
    }
};

export const getUserTestingFail = (error) => {
    return {
        type: actionTypes.GET_USER_TESTING_FAIL,
        error: error
    };
};

export const closeModalErrorgetUserTesting = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_USER_TESTING
    };
};

export const getUserTesting =(accountId)=> {
    return dispatch => {
        dispatch(getUserTestingStart());
    
        const config = {
          headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/testing/user/${accountId}`, config)
        .then(response => {
            dispatch(getUserTestingSuccess(response.data.data, response.data.isSuccess));
            console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getUserTestingFail(error.response.data.message));
            }
        });
    };
};