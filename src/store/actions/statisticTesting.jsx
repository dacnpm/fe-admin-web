import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const statisticTestingStart = () => {
    return {
        type: actionTypes.STATISTIC_TESTING_START
    };
};

export const statisticTestingSuccess = (data, isSuccess) => {
    return {
        type: actionTypes.STATISTIC_TESTING_SUCCESS,
        data: data,
        isSuccess: isSuccess
    }
};

export const statisticTestingFail = (error) => {
    return {
        type: actionTypes.STATISTIC_TESTING_FAIL,
        error: error
    };
};

export const closeModalErrorStatisticTesting = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_STATISTIC_TESTING
    };
};

export const statisticTesting =(locationId)=> {
    return dispatch => {
        dispatch(statisticTestingStart());

        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/testing/statistic?testingLocationId=${locationId}`, config)
        .then(response => {
            dispatch(statisticTestingSuccess(response.data.data, response.data.isSuccess));
            console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(statisticTestingFail(error.response.data.message));
            }
        });
    };
};