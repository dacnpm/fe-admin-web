export {
    login,
    closeModalErrorLogin
} from './login';

export {
    confirm,
    closeModalErrorConfirm
} from './confirmData'

export {
    getTesting,
    closeModalErrorGetTesting
} from './getTesting'

export {
    updateTesting,
    closeModalErrorUpdateTesting
} from './updateTestingState'

export {
    getLocation,
    closeModalErrorGetLocation
} from './getLocation'

export {
    statisticTesting,
    closeModalErrorStatisticTesting
} from './statisticTesting'

export {
    getTestingDetail,
    closeModalErrorgetTestingDetail
} from './getTestingDetail'

export {
    statisticItinerary,
    closeModalErrorStatisticItinerary
} from './statisticItinerary'

export {
    getCity,
    closeModalErrorGetCity
} from './getCityList'

export {
    updateCity,
    closeModalErrorUpdateCity
} from './updateCity'

export {
    addCity,
    closeModalErrorAddCity
} from './addCity'

export {
    deleteCity,
    closeModalErrorDeleteCity
} from './deleteCity'

export {
    addLocation,
    closeModalErrorAddLocation
} from './addTestingLocation'

export {
    updateLocation,
    closeModalErrorUpdateLocation
} from './updateTestingLocation'

export {
    deleteLocation,
    closeModalErrorDeleteLocation
} from './deleteTestingLocation'

export {
    getUsers,
    closeModalErrorGetUsers
} from './getUserList'

export {
    getUserTesting,
    closeModalErrorgetUserTesting
} from './getUserTesting'

export {
    getUserCheckin,
    closeModalErrorgetUserCheckin
} from './getUserCheckin'

export {
    getUserItinerary,
    closeModalErrorgetUserItinerary
} from './getUserItinerary'

export {
    getNews,
    closeModalErrorGetNews
} from './getNews'

export {
    getNewsDetail,
    closeModalErrorgetNewsDetail
} from './getNewsDetail'

export {
    addNews,
    closeModalErrorAddNews
} from './addNews'

export {
    updateNews,
    closeModalErrorUpdateNews
} from './updateNews'

export {
    deleteNews,
    closeModalErrorDeleteNews
} from './deleteNews'

export {
    uploadImage,
    closeModalErrorUploadImage
} from './uploadImage'
