import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getUserCheckinStart = () => {
    return {
        type: actionTypes.GET_USER_CHECKIN_START
    };
};

export const getUserCheckinSuccess = (data) => {
    return {
        type: actionTypes.GET_USER_CHECKIN_SUCCESS,
        data: data
    }
};

export const getUserCheckinFail = (error) => {
    return {
        type: actionTypes.GET_USER_CHECKIN_FAIL,
        error: error
    };
};

export const closeModalErrorgetUserCheckin= () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_USER_CHECKIN
    };
};

export const getUserCheckin =(accountId)=> {
    return dispatch => {
        dispatch(getUserCheckinStart());
    
        const config = {
          headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/location-checkin/${accountId}`, config)
        .then(response => {
            dispatch(getUserCheckinSuccess(response.data.data, response.data.isSuccess));
            console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getUserCheckinFail(error.response.data.message));
            }
        });
    };
};