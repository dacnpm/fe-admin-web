import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const updateNewsStart = () => {
    return {
        type: actionTypes.UPDATE_NEWS_START
    };
};

export const updateNewsSuccess = (data) => {
    return {
        type: actionTypes.UPDATE_NEWS_SUCCESS,
        data: data,
        isSuccess: data.isSuccess
    }
};

export const updateNewsFail = (error) => {
    return {
        type: actionTypes.UPDATE_NEWS_FAIL,
        error: error
    };
};

export const closeModalErrorUpdateNews = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_UPDATE_NEWS
    };
};

export const updateNews =(news)=> {
    return dispatch => {
        dispatch(updateNewsStart());
        
        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.put(`/Homepage/news/${news.id}`,news,config)
        .then(response => {
            dispatch(updateNewsSuccess(response.data));
            //console.log(response.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(updateNewsFail(error.response));
            }
        });
    };
};