import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getCityStart = () => {
    return {
        type: actionTypes.GET_CITY_START
    };
};

export const getCitySuccess = (data, isSuccess) => {
    return {
        type: actionTypes.GET_CITY_SUCCESS,
        data: data,
        isSuccess: isSuccess
    }
};

export const getCityFail = (error) => {
    return {
        type: actionTypes.GET_LOCATION_FAIL,
        error: error
    };
};

export const closeModalErrorGetCity = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_CITY
    };
};

export const getCity =()=> {
    return dispatch => {
        dispatch(getCityStart());

        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/city`, config)
        .then(response => {
            dispatch(getCitySuccess(response.data.data,response.data.isSuccess));
            //console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getCityFail(error.response.data.message));
            }
        });
    };
};