import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const addCityStart = () => {
    return {
        type: actionTypes.ADD_CITY_START
    };
};

export const addCitySuccess = (data) => {
    return {
        type: actionTypes.ADD_CITY_SUCCESS,
        data: data,
        isSuccess: data.isSuccess
    }
};

export const addCityFail = (error) => {
    return {
        type: actionTypes.ADD_CITY_FAIL,
        error: error
    };
};

export const closeModalErrorAddCity = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_ADD_CITY
    };
};

export const addCity =(city)=> {
    return dispatch => {
        dispatch( addCityStart());
    
        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.post(`/city`,city,config)
        .then(response => {
            dispatch(addCitySuccess(response.data));
            console.log(response);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(addCityFail(error.response));
            }
        });
    };
};