import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getLocationStart = () => {
    return {
        type: actionTypes.GET_LOCATION_START
    };
};

export const getLocationSuccess = (data, isSuccess) => {
    return {
        type: actionTypes.GET_LOCATION_SUCCESS,
        data: data,
        isSuccess: isSuccess
    }
};

export const getLocationFail = (error) => {
    return {
        type: actionTypes.GET_LOCATION_FAIL,
        error: error
    };
};

export const closeModalErrorGetLocation = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_LOCATION
    };
};

export const getLocation =()=> {
    return dispatch => {
        dispatch(getLocationStart());

        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/testing/location`, config)
        .then(response => {
            dispatch(getLocationSuccess(response.data.data, response.data.isSuccess));
            //console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getLocationFail(error.response.data.message));
            }
        });
    };
};