import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getUserItineraryStart = () => {
    return {
        type: actionTypes.GET_USER_ITINERARY_START
    };
};

export const getUserItinerarySuccess = (data) => {
    return {
        type: actionTypes.GET_USER_ITINERARY_SUCCESS,
        data: data
    }
};

export const getUserItineraryFail = (error) => {
    return {
        type: actionTypes.GET_USER_ITINERARY_FAIL,
        error: error
    };
};

export const closeModalErrorgetUserItinerary= () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_USER_ITINERARY
    };
};

export const getUserItinerary =(email)=> {
    return dispatch => {
        dispatch(getUserItineraryStart());
    
        const config = {
          headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/itineraries/user?email=${email}&&PageSize=${100}`, config)
        .then(response => {
            dispatch(getUserItinerarySuccess(response.data.data, response.data.isSuccess));
            //console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getUserItineraryFail(error.response.data.message));
            }
        });
    };
};