import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const deleteNewsStart = () => {
    return {
        type: actionTypes.DELETE_CITY_START
    };
};

export const deleteNewsSuccess = (data) => {
    return {
        type: actionTypes.DELETE_CITY_SUCCESS,
        data: data,
        isSuccess: data.data.isSuccess
    }
};

export const deleteNewsFail = (error) => {
    return {
        type: actionTypes.DELETE_CITY_FAIL,
        error: error
    };
};

export const closeModalErrorDeleteNews = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_DELETE_CITY
    };
};

export const deleteNews =(newsId)=> {
    return dispatch => {
        dispatch(deleteNewsStart());
    
        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.delete(`/Homepage/news/${newsId}`,config)
        .then(response => {
            dispatch(deleteNewsSuccess(response.data));
            console.log(response);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(deleteNewsFail(error.response));
            }
        });
    };
};