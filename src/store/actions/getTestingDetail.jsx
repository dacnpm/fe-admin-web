import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const getTestingDetailStart = () => {
    return {
        type: actionTypes.GET_TESTING_DETAIL_START
    };
};

export const getTestingDetailSuccess = (data) => {
    return {
        type: actionTypes.GET_TESTING_DETAIL_SUCCESS,
        data: data
    }
};

export const getTestingDetailFail = (error) => {
    return {
        type: actionTypes.GET_TESTING_DETAIL_FAIL,
        error: error
    };
};

export const closeModalErrorgetTestingDetail = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_GET_TESTING_DETAIL
    };
};

export const getTestingDetail =(testingId)=> {
    return dispatch => {
        dispatch(getTestingDetailStart());
    
        const config = {
          headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/testing/${testingId}`, config)
        .then(response => {
            dispatch(getTestingDetailSuccess(response.data.data, response.data.isSuccess));
            console.log(response.data.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(getTestingDetailFail(error.response.data.message));
            }
        });
    };
};