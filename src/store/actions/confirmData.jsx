import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const confirmStart = () => {
    return {
        type: actionTypes.CONFIRM_START
    };
};

export const confirmSuccess = (isSuccess, email, departure, destination, departureTime, landingTime, travelNo) => {
    return {
        type: actionTypes.CONFIRM_SUCCESS,
        isSuccess: isSuccess,
        email : email,
        departure: departure,
        destination: destination,
        departureTime: departureTime,
        landingTime: landingTime,
        travelNo: travelNo
    }
};

export const confirmFail = (error) => {
    return {
        type: actionTypes.CONFIRM_FAIL,
        error: error
    };
};

export const closeModalErrorConfirm = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_CONFIRM
    };
};

export const confirm = (email) => {
    return dispatch => {
        dispatch(confirmStart());

        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.get(`/Admin/itinerary/user?email=${email}`, config)
        .then(response => {
            dispatch(confirmSuccess(response.data.isSuccess, response.data.data.email, response.data.data.departure,
            response.data.data.destination, response.data.data.departureTime, response.data.data.landingTime,response.data.data.travelNo));
            console.log(response.data);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(confirmFail(error.response.data.message));
            }
        });
    };
};