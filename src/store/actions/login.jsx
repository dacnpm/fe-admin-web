import * as actionTypes from './actionTypes';
import axios from '../../axios-base';
import moment from 'moment';
import '../utility';
import { handleTimeout } from './../utility';

export const loginStart = () => {
    return {
        type: actionTypes.LOGIN_START
    };
};

export const loginSuccess = (isSuccess, userId, token, fullName, role) => {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        isSuccess: isSuccess,
        userId: userId,
        token: token,
        fullName: fullName,
        role: role
    };
};

export const loginFail = (error, errorCode) => {
    return {
        type: actionTypes.LOGIN_FAIL,
        error: error,
        errorCode: errorCode
    };
};

export const closeModalErrorLogin = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_LOGIN
    };
};

export const login = (email, password) => {
    return dispatch => {
        dispatch(loginStart());
        const loginData = {
        email: email,
        password: password
        };
        axios.post("/Authorization/admin/login", loginData)
        .then(response => {
            dispatch(loginSuccess(response.data.isSuccess, response.data.data.id, response.data.data.token, response.data.data.fullName, response.data.data.roles));
            localStorage.setItem('token', response.data.data.token);
            localStorage.setItem('roles', response.data.data.roles[0].guardName);
            localStorage.setItem('user', response.data.data.fullName);
            let d = new Date(moment());
            d.setTime(d.getTime() + 1);
            console.log(response.data);
        })
        .catch(error => {
            localStorage.clear();
            if (error.response) {
                console.log(error.response)
                dispatch(loginFail(error.response.data.message, error.response.data.code));
            } else {
                // console.log(error)
                handleTimeout(error);    
            }
        });
    };
};