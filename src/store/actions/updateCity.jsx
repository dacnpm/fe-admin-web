import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const updateCityStart = () => {
    return {
        type: actionTypes.UPDATE_CITY_START
    };
};

export const updateCitySuccess = (data) => {
  return {
        type: actionTypes.UPDATE_CITY_SUCCESS,
        data: data,
        isSuccess: data.isSuccess
    }
};

export const updateCityFail = (error) => {
    return {
        type: actionTypes.UPDATE_CITY_FAIL,
        error: error
    };
};

export const closeModalErrorUpdateCity = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_UPDATE_CITY
    };
};

export const updateCity =(city)=> {
    return dispatch => {
        dispatch(updateCityStart());
        
        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.put(`/city/${city.id}`,city,config)
        .then(response => {
            dispatch(updateCitySuccess(response.data));
            console.log(response);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(updateCityFail(error.response));
            }
        });
    };
};