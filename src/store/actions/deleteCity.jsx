import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const deleteCityStart = () => {
    return {
        type: actionTypes.DELETE_CITY_START
    };
};

export const deleteCitySuccess = (data) => {
    return {
        type: actionTypes.DELETE_CITY_SUCCESS,
        data: data
    }
};

export const deleteCityFail = (error) => {
    return {
        type: actionTypes.DELETE_CITY_FAIL,
        error: error
    };
};

export const closeModalErrorDeleteCity = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_DELETE_CITY
    };
};

export const deleteCity =(cityId)=> {
    deleteCityStart();
    
    const config = {
        headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
    };
    axios.delete(`/city/${cityId}`,config)
    .then(response => {
        return deleteCitySuccess(response.data.isSuccess);
    })
    .catch(error => {
        if(error.response === undefined){
            setTimeout(() => {
                window.location = "/network-error";
            }, 1500)
        }else{
            return deleteCityFail(error.response);
        }
    });
};