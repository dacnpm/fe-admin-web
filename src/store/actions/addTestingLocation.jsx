import * as actionTypes from './actionTypes';
import axios from '../../axios-base';

export const addLocationStart = () => {
    return {
        type: actionTypes.ADD_LOCATION_START
    };
};

export const addLocationSuccess = (data) => {
    return {
        type: actionTypes.ADD_LOCATION_SUCCESS,
        data: data,
        isSuccess: data.isSuccess
    }
};

export const addLocationFail = (error) => {
    return {
        type: actionTypes.ADD_LOCATION_FAIL,
        error: error
    };
};

export const closeModalErrorAddLocation = () => {
    return {
        type: actionTypes.CLOSE_MODAL_ERROR_ADD_LOCATION
    };
};

export const addLocation =(location)=> {
    return dispatch => {
        dispatch(addLocationStart());
        
        const config = {
            headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
        };
        axios.post(`testing/location`,location,config)
        .then(response => {
            dispatch(addLocationSuccess(response.data));
            console.log(response);
        })
        .catch(error => {
            if(error.response === undefined){
                setTimeout(() => {
                    window.location = "/network-error";
                }, 1500)
            }else{
                dispatch(addLocationFail(error.response));
            }
        });
    };
};