import moment from 'moment';

export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const dateTimeFormat = (date, format) => {
var dateFormat = require("dateformat");

    dateFormat.i18n = {
        dayNames: [
            "CN",
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7",
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
        ],
        monthNames: [
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ],
        timeNames: ["a", "p", "am", "pm", "A", "P", "AM", "PM"],
    };

    return dateFormat(moment(date), format);
}

export const handleTimeout = (error) =>
{
    if (error.code === 'ECONNABORTED') {
        window.location = "/network-error";
    }else{
        window.location = "/network-error";
    }
    console.log(error.code);
}