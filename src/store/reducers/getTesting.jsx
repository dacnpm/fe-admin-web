import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null,
    isSuccess: false
};

const getTestingStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getTestingSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccesss
    });
};

const getTestingFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorGetTesting = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_TESTING_START:
            return getTestingStart(state, action);
        case actionTypes.GET_TESTING_SUCCESS:
            return getTestingSuccess(state, action);
        case actionTypes.GET_TESTING_FAIL:
            return getTestingFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_TESTING:
            return closeModalErrorGetTesting(state, action);
        default:
            return state
    }
}
export default reducer;