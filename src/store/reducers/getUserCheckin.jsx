import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const getUserCheckinStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getUserCheckinSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error
    });
};

const getUserCheckinFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorgetUserCheckin = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_USER_CHECKIN_START:
            return getUserCheckinStart(state, action);
        case actionTypes.GET_USER_CHECKIN_SUCCESS:
            return getUserCheckinSuccess(state, action);
        case actionTypes.GET_USER_CHECKIN_FAIL:
            return getUserCheckinFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_USER_CHECKIN:
            return closeModalErrorgetUserCheckin(state, action);
        default:
            return state
    }
}
export default reducer;