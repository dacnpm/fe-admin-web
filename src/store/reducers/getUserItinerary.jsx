import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const getUserItineraryStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getUserItinerarySuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error
    });
};

const getUserItineraryFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorgetUserItinerary = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_USER_ITINERARY_START:
            return getUserItineraryStart(state, action);
        case actionTypes.GET_USER_ITINERARY_SUCCESS:
            return getUserItinerarySuccess(state, action);
        case actionTypes.GET_USER_ITINERARY_FAIL:
            return getUserItineraryFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_USER_ITINERARY:
            return closeModalErrorgetUserItinerary(state, action);
        default:
            return state
    }
}
export default reducer;