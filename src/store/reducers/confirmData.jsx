import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    isSuccess: false,
    email: null,
    departure: null,
    destination: null,
    departureTime: null,
    landingTime: null,
    travelNo: null,
    error: null
};

const confirmStart = (state, action) => {
    return updateObject(state, { error: null});
};

const confirmSuccess = (state, action) => {
    return updateObject(state, {
        isSuccess: action.isSuccess,
        email: action.email,
        departure: action.departure,
        destination: action.destination,
        departureTime: action.departureTime,
        landingTime: action.landingTime,
        travelNo: action.travelNo,
        error:   action.error
    });
};

const confirmFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorConfirm = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CONFIRM_START:
            return confirmStart(state, action);
        case actionTypes.CONFIRM_SUCCESS:
            return confirmSuccess(state, action);
        case actionTypes.CONFIRM_FAIL:
            return confirmFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_CONFIRM:
            return closeModalErrorConfirm(state, action);
        default:
            return state
    }
}
export default reducer;