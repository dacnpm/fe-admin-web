import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const getNewsDetailStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getNewsDetailSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const getNewsDetailFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorgetNewsDetail = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_NEWS_DETAIL_START:
            return getNewsDetailStart(state, action);
        case actionTypes.GET_NEWS_DETAIL_SUCCESS:
            return getNewsDetailSuccess(state, action);
        case actionTypes.GET_NEWS_DETAIL_FAIL:
            return getNewsDetailFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_NEWS_DETAIL:
            return closeModalErrorgetNewsDetail(state, action);
        default:
            return state
    }
}
export default reducer;