import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null,
    isSuccess: false
};

const getUsersStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getUsersSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccesss
    });
};

const getUsersFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorGetUsers = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_USERS_START:
            return getUsersStart(state, action);
        case actionTypes.GET_USERS_SUCCESS:
            return getUsersSuccess(state, action);
        case actionTypes.GET_USERS_FAIL:
            return getUsersFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_USERS:
            return closeModalErrorGetUsers(state, action);
        default:
            return state
    }
}
export default reducer;