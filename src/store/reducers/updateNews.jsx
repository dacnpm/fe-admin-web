import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const updateNewsStart = (state, action) => {
    return updateObject(state, { error: null});
};

const updateNewsSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const updateNewsFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorUpdateNews = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_NEWS_START:
            return updateNewsStart(state, action);
        case actionTypes.UPDATE_NEWS_SUCCESS:
            return updateNewsSuccess(state, action);
        case actionTypes.UPDATE_NEWS_FAIL:
            return updateNewsFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_UPDATE_NEWS:
            return closeModalErrorUpdateNews(state, action);
        default:
            return state
    }
}
export default reducer;