import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const updateTestingStart = (state, action) => {
    return updateObject(state, { error: null});
};

const updateTestingSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error
    });
};

const updateTestingFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorUpdateTesting = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_TESTING_START:
            return updateTestingStart(state, action);
        case actionTypes.UPDATE_TESTING_SUCCESS:
            return updateTestingSuccess(state, action);
        case actionTypes.UPDATE_TESTING_FAIL:
            return updateTestingFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_UPDATE_TESTING:
            return closeModalErrorUpdateTesting(state, action);
        default:
            return state
    }
}
export default reducer;