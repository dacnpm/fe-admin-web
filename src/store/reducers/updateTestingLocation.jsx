import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const updateTestingLocationStart = (state, action) => {
    return updateObject(state, { error: null});
};

const updateTestingLocationSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const updateTestingLocationFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorUpdateTestingLocation = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_LOCATION_START:
            return updateTestingLocationStart(state, action);
        case actionTypes.UPDATE_LOCATION_SUCCESS:
            return updateTestingLocationSuccess(state, action);
        case actionTypes.UPDATE_LOCATION_FAIL:
            return updateTestingLocationFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_UPDATE_LOCATION:
            return closeModalErrorUpdateTestingLocation(state, action);
        default:
            return state
    }
}
export default reducer;