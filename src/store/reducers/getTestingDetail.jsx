import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const getTestingDetailStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getTestingDetailSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error
    });
};

const getTestingDetailFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorgetTestingDetail = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_TESTING_DETAIL_START:
            return getTestingDetailStart(state, action);
        case actionTypes.GET_TESTING_DETAIL_SUCCESS:
            return getTestingDetailSuccess(state, action);
        case actionTypes.GET_TESTING_DETAIL_FAIL:
            return getTestingDetailFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_TESTING_DETAIL:
            return closeModalErrorgetTestingDetail(state, action);
        default:
            return state
    }
}
export default reducer;