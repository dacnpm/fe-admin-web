import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const updateCityStart = (state, action) => {
    return updateObject(state, { error: null});
};

const updateCitySuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const updateCityFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorUpdateCity = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_CITY_START:
            return updateCityStart(state, action);
        case actionTypes.UPDATE_CITY_SUCCESS:
            return updateCitySuccess(state, action);
        case actionTypes.UPDATE_CITY_FAIL:
            return updateCityFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_UPDATE_CITY:
            return closeModalErrorUpdateCity(state, action);
        default:
            return state
    }
}
export default reducer;