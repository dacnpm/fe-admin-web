import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const getUserTestingStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getUserTestingSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error
    });
};

const getUserTestingFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorgetUserTesting = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_USER_TESTING_START:
            return getUserTestingStart(state, action);
        case actionTypes.GET_USER_TESTING_SUCCESS:
            return getUserTestingSuccess(state, action);
        case actionTypes.GET_USER_TESTING_FAIL:
            return getUserTestingFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_USER_TESTING:
            return closeModalErrorgetUserTesting(state, action);
        default:
            return state
    }
}
export default reducer;