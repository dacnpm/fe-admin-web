import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const statisticItineraryStart = (state, action) => {
    return updateObject(state, { error: null});
};

const statisticItinerarySuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        isSuccess: action.isSuccess,
        error:   action.error
    });
};

const statisticItineraryFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorStatisticItinerary = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STATISTIC_ITINERARY_START:
            return statisticItineraryStart(state, action);
        case actionTypes.STATISTIC_ITINERARY_SUCCESS:
            return statisticItinerarySuccess(state, action);
        case actionTypes.STATISTIC_ITINERARY_FAIL:
            return statisticItineraryFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_STATISTIC_ITINERARY:
            return closeModalErrorStatisticItinerary(state, action);
        default:
            return state
    }
}
export default reducer;