import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const addTestingLocationStart = (state, action) => {
    return updateObject(state, { error: null});
};

const addTestingLocationSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const addTestingLocationFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorAddTestingLocation = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_LOCATION_START:
            return addTestingLocationStart(state, action);
        case actionTypes.ADD_LOCATION_SUCCESS:
            return addTestingLocationSuccess(state, action);
        case actionTypes.ADD_LOCATION_FAIL:
            return addTestingLocationFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_ADD_LOCATION:
            return closeModalErrorAddTestingLocation(state, action);
        default:
            return state
    }
}
export default reducer;