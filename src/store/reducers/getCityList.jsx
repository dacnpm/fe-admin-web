import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const getCityStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getCitySuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const getCityFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorGetCity = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_CITY_START:
            return getCityStart(state, action);
        case actionTypes.GET_CITY_SUCCESS:
            return getCitySuccess(state, action);
        case actionTypes.GET_CITY_FAIL:
            return getCityFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_CITY:
            return closeModalErrorGetCity(state, action);
        default:
            return state
    }
}
export default reducer;