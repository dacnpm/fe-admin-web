import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const deleteNewsStart = (state, action) => {
    return updateObject(state, { error: null});
};

const deleteNewsSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const deleteNewsFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorDeleteNews = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.DELETE_NEWS_START:
            return deleteNewsStart(state, action);
        case actionTypes.DELETE_NEWS_SUCCESS:
            return deleteNewsSuccess(state, action);
        case actionTypes.DELETE_NEWS_FAIL:
            return deleteNewsFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_DELETE_NEWS:
            return closeModalErrorDeleteNews(state, action);
        default:
            return state
    }
}
export default reducer;