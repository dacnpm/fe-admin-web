import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const uploadImageStart = (state, action) => {
    return updateObject(state, { error: null});
};

const uploadImageSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const uploadImageFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErroruploadImage = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPLOAD_IMAGE_START:
            return uploadImageStart(state, action);
        case actionTypes.UPLOAD_IMAGE_SUCCESS:
            return uploadImageSuccess(state, action);
        case actionTypes.UPLOAD_IMAGE_FAIL:
            return uploadImageFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_UPLOAD_IMAGE:
            return closeModalErroruploadImage(state, action);
        default:
            return state
    }
}
export default reducer;