import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null,
    isSuccess: false
};

const getNewsStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getNewsSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccesss
    });
};

const getNewsFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorGetNews = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_NEWS_START:
            return getNewsStart(state, action);
        case actionTypes.GET_NEWS_SUCCESS:
            return getNewsSuccess(state, action);
        case actionTypes.GET_NEWS_FAIL:
            return getNewsFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_NEWS:
            return closeModalErrorGetNews(state, action);
        default:
            return state
    }
}
export default reducer;