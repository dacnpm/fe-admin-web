import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const statisticTestingStart = (state, action) => {
    return updateObject(state, { error: null});
};

const statisticTestingSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        isSuccess: action.isSuccess,
        error:   action.error
    });
};

const statisticTestingFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorStatisticTesting = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STATISTIC_TESTING_START:
            return statisticTestingStart(state, action);
        case actionTypes.STATISTIC_TESTING_SUCCESS:
            return statisticTestingSuccess(state, action);
        case actionTypes.STATISTIC_TESTING_FAIL:
            return statisticTestingFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_STATISTIC_TESTING:
            return closeModalErrorStatisticTesting(state, action);
        default:
            return state
    }
}
export default reducer;