import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const addCityStart = (state, action) => {
    return updateObject(state, { error: null});
};

const addCitySuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const addCityFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErroraddCity = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_CITY_START:
            return addCityStart(state, action);
        case actionTypes.ADD_CITY_SUCCESS:
            return addCitySuccess(state, action);
        case actionTypes.ADD_CITY_FAIL:
            return addCityFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_ADD_CITY:
            return closeModalErroraddCity(state, action);
        default:
            return state
    }
}
export default reducer;