import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const getLocationStart = (state, action) => {
    return updateObject(state, { error: null});
};

const getLocationSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const getLocationFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErrorGetLocation = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_LOCATION_START:
            return getLocationStart(state, action);
        case actionTypes.GET_LOCATION_SUCCESS:
            return getLocationSuccess(state, action);
        case actionTypes.GET_LOCATION_FAIL:
            return getLocationFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_GET_LOCATION:
            return closeModalErrorGetLocation(state, action);
        default:
            return state
    }
}
export default reducer;