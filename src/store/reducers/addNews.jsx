import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null,
    error: null
};

const addNewsStart = (state, action) => {
    return updateObject(state, { error: null});
};

const addNewsSuccess = (state, action) => {
    return updateObject(state, {
        data: action.data,
        error:   action.error,
        isSuccess: action.isSuccess
    });
};

const addNewsFail = (state, action) => {
    return updateObject(state, {
        isSuccess: false,
        error: action.error,
    });
};

const closeModalErroraddNews = (state, action) => {
    return updateObject(state, { error: null });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_NEWS_START:
            return addNewsStart(state, action);
        case actionTypes.ADD_NEWS_SUCCESS:
            return addNewsSuccess(state, action);
        case actionTypes.ADD_NEWS_FAIL:
            return addNewsFail(state, action);
        case actionTypes.CLOSE_MODAL_ERROR_ADD_NEWS:
            return closeModalErroraddNews(state, action);
        default:
            return state
    }
}
export default reducer;