import sidebarImg from '../../assets/images/chong-corona5.jpg';

const SidebarBackground = () => {
    return ( 
        <div class="sidebarImg">
                <img src={sidebarImg} alt="my-sidebarImg" />
        </div>
     );
}
 
export default SidebarBackground;