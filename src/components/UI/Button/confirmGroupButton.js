import React , {useState} from "react";
import {Button} from "@material-ui/core"; 
import classes from './Button.module.css';
import * as actions from '../../../store/actions/index';

export default function ConfirmGroupButton(props){
    const [state, setState ] = useState(props.status);

    const handleChangeState = (testing) => {
        setState(1);
        testing.testingState = 1;
        testing.result = false ;
        testing.isPaid = false ;
        actions.updateTesting(testing.id, testing);
    }

    var content = '';

        content =  <div className = {classes.content}>
                        <Button variant="contained" color="primary" onClick = {()=> handleChangeState(props.testing)} >
                            Xác nhận
                        </Button>
                    </div>
    return (
        content
    )
}
 

