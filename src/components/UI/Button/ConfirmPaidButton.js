import React , {useState,useEffect} from "react";
import * as actions from '../../../store/actions/index';
import ToggleButton from 'react-bootstrap/ToggleButton'
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup'
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';

export default function ConfirmIsPaidButton(props){
    const [isPaid , setPaidState ] = useState();

    useEffect(() =>{
        setPaidState(props.isPaid);
    },[props.isPaid])

    var testing = props.testing;
    let result = false;
    if(testing.testingStateId === 1) {
        result = false;
    }else if(testing.testingStateId === 2) {
        if(testing.result === "Negative"){
            result = false
        }else {
            result = true;
        }
    }
    const handleChangePaidState = (event) => {
        var updateData = {
            id: testing.id,
            isPaid: event.target.value,
            testingState : testing.testingStateId,
            result: result
        } 
        actions.updateTesting(updateData.id,updateData);
    }

    var content = '';

    if(isPaid){
        content = <ToggleButtonGroup type="radio"  name="1">
                    <ToggleButton value={true} size="sm" variant="success"
                        onChange = {handleChangePaidState}>
                        <DoneIcon />
                    </ToggleButton>
                    <ToggleButton value={false}  size="sm"variant="secondary"
                        onChange = {handleChangePaidState}>
                        <CloseIcon/>
                    </ToggleButton>
                </ToggleButtonGroup>
    }else  {
        content = <ToggleButtonGroup type="radio"  name="1">
                    <ToggleButton value={true} size="sm" variant="secondary"
                        onChange = {handleChangePaidState}>
                        <DoneIcon />
                    </ToggleButton>
                    <ToggleButton value={false}  size="sm"variant="danger"
                        onChange = {handleChangePaidState}>
                        <CloseIcon/>
                    </ToggleButton>
                    </ToggleButtonGroup>
    }

    return (
        content
    )
}
 

