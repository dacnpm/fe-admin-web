import React , {useState} from "react";
import {Button} from "@material-ui/core"; 
import styles from './Button.module.css';
import * as actions from '../../../store/actions/index';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

function rand() {
    return Math.round(Math.random() * 20) - 10;
  }
  
function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();
    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}
  
const useStyles = makeStyles((theme) => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export default function TestingResultButton(props){
    const [testing, setTesting ] = useState(props.testing);
    const updateData = props.testing;
    const [newResult, setResult] = useState(true);
    const classes = useStyles();

    const [modalStyle] = React.useState(getModalStyle);
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChangeResult = (event) => {
        const result = event.target.value;
        //alert(result);
        setResult(result);
    };

    const handleUpdateTestingResult = () => {
        //alert(newResult);
        updateData.testingState = 2;
        updateData.result = newResult;
        var resultTxt = newResult === true ? "Positive": "Negative";
        setTesting({
            ...testing,
            ["testingStateId"]: 2,
            ["result"]: resultTxt,
        });
        actions.updateTesting(updateData.id, updateData);
        handleClose();
        //alert("Cập nhập kết quả thành công");
    }

    const body = (
        <div style={modalStyle} className={classes.paper}>
            <h2 id="simple-modal-title">Cập nhật kết quả XN</h2>
            <table>
                <tr>
                    <th>Thông tin xét nghiệm</th>
                </tr>
                <tr>
                    <td>Họ tên:</td>
                    <td>{testing.fullName}</td>
                </tr>
                <tr>
                    <td>Ngày xét nghiệm:</td>
                    <td>{testing.testingDate}</td>
                </tr>
                <tr>
                    <td>Địa điểm:</td>
                    <td>{testing.testingLocation.name}</td>
                </tr>
                <tr>
                    <th>Cập nhật kết quả</th>
                    <tr>
                        <FormControl>
                            <Select
                                native
                                onChange={(event) => handleChangeResult(event)}
                                >
                                <option value={true}>Positive</option>
                                <option value={false}>Negative</option>
                            </Select>
                            <button className = "btn btn-sm btn-success" onClick = {() => handleUpdateTestingResult()}>Cập nhập</button>
                        </FormControl>
                    </tr>
                </tr>
            </table>
        </div>
    );


    var content = '';
    if(testing.testingStateId === 2){
        if(testing.result === "Positive"){
            content =  <div className = {styles.content}>
                            <Button variant="contained" color="primary"  onClick={()=>handleOpen()} >
                                Cập nhập
                            </Button>
                            <Modal
                                open={open}
                                onClose={handleClose}
                                aria-labelledby="simple-modal-title"
                                aria-describedby="simple-modal-description"
                                >
                                {body}
                            </Modal>
                        </div>
        }else {
            content =  <div className = {styles.content}>
                            <Button variant="contained" color="primary"  onClick={()=>handleOpen()} >
                                Cập nhập
                            </Button>
                            <Modal
                                open={open}
                                onClose={handleClose}
                                aria-labelledby="simple-modal-title"
                                aria-describedby="simple-modal-description"
                                >
                                {body}
                            </Modal>
                        </div>
        }
    }else if(testing.testingStateId === 1){
        content =  <div className = {styles.content}>
                        <Button variant="contained" color="primary"  onClick={()=>handleOpen()} >
                            Cập nhập
                        </Button>
                        <Modal
                            open={open}
                            onClose={handleClose}
                            aria-labelledby="simple-modal-title"
                            aria-describedby="simple-modal-description"
                            >
                            {body}
                        </Modal>
                    </div>
    }
    
    return (
        content
    )
}
 

