import React ,{ Fragment }from 'react';
import covidLogo from '../../assets/images/stop_covid.png';
import './Logo.css';

const Logo = (props) => {
    return ( 
        <Fragment>
            <div class="logo">
                <img src={covidLogo} alt="my-logo" />
            </div>
        </Fragment>
     );
}
 
export default Logo;