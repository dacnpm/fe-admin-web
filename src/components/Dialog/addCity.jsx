import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import { useEffect } from 'react';
import * as actions from '../../store/actions/index';
import classes from './EditCity.module.css'
import { connect } from 'react-redux';

function AddCityDialog(props) {
    const [open, setOpen] = React.useState(false);
    let [cityName, setCityname] = React.useState("");
    let [cityStatus, setCityStatus] = React.useState("Negative");
    let [errorName, setErrorName] = React.useState("");
    let city = {};
    let cityList = props.cityList;
    let [isChangeName, setIsChangeName] = React.useState(false);
  
    useEffect(() =>{
        setOpen(props.open);
        setIsChangeName(false);
        setErrorName("");
    },[props.open]);
    
    const handleChangeName = (event) => {
        setIsChangeName(true);
        const value = event.target.value;
        if(value !== ""){
            setCityname(value);
            setErrorName("");
        }else {
            setErrorName("Vui lòng nhập tên tỉnh/thành !");
        }
    }

    const handleChangeStatus = (event) => {
        const value = event.target.value;
        setCityStatus(value);
    }

    const checkExistName = (cityName) => {
        var cityExist = cityList.filter(row => row.name.toLowerCase().indexOf(cityName.toLowerCase()) !== -1);
        if(cityExist.length === 0){
            return false
        }else {
            return true;
        }
    }
     
    const handleSubmitAdd = (event) => {
        if(isChangeName && errorName === ""){
            if(checkExistName(cityName)){
                setErrorName("Tên tỉnh/thành đã tồn tại !")
            }else {
            city.name = cityName;
            city.status = cityStatus;
            props.onAddCity(city);
            if(props.isSuccess){
                props.onClose();
            }else {
                alert("Thêm thất bại !")
            }
            }
        }else if(!isChangeName || errorName !== "") {
            setErrorName("Vui lòng nhập tên tỉnh/thành !")
        }
    }

    const handleClose = () => {
        props.onClose();
    }

    return (
        <div>
            <Dialog open={open} onClose={() => handleClose()} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Thêm tỉnh/thành</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        
                    </DialogContentText>
                    <p className = {classes.error}>{errorName}</p>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Tên tỉnh/thành"
                        type="email"
                        fullWidth
                        onChange = {(event) => handleChangeName(event)}
                    />
                    <p>Chọn tình trạng: </p>
                    <Select
                        defaultValue = "Negative"
                        onChange={(event) => handleChangeStatus(event)}
                        >
                        <option value="Negative">An toàn</option>
                        <option value="Positive">Đang có dịch</option>
                    </Select>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.onClose()} color="primary">
                        Hủy
                    </Button>
                    <Button onClick={() =>  handleSubmitAdd()} color="primary">
                        Lưu
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        data: state.addCity.data,
        isSuccess: state.addCity.isSuccess,
        error: state.addCity.error,
        isEditSuccess: state.updateCity.isSuccess
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddCity: (city) => dispatch(actions.addCity(city)),
        onEditCity: (city) => dispatch(actions.updateCity(city))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCityDialog);
