import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import { useEffect } from 'react';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';

function EditLocationDialog(props) {
    const [open, setOpen] = React.useState(false);
    let [locationName, setLocationName] = React.useState("");
    let [locationAddress, setLocationAddress] = React.useState("");
    let [location,setLocation] = React.useState();
    let [cities, setCity] = React.useState([]);
    let [cityId, setCityId] = React.useState(0);
  
    useEffect(() =>{
        setOpen(props.open);
        setLocation(props.location);
        props.onGetCityList();
        if(props.data){
            setCity(props.data);
        }
    },[props.location, props.open]);
    
    const handleChangeName = (event) => {
        const value = event.target.value;
        setLocationName(value);
    }

    const handleChangeAddress = (event) => {
        const value = event.target.value;
        setLocationAddress(value);
    }
     
    const handleSubmitAdd = (event) => {
        location.name = locationName;
        location.address = locationAddress;
        location.cityId = cityId;
        actions.addLocation(location);
        setLocationName("");
        setLocationAddress("");
        setCityId(0);
        return props.onClose();

    }

    const handleChangeCity = (event)=> {
        const value = event.target.value;
        setCityId(value);
    }

    const handleSubmitEdit = (event) => {
        if(locationName === "") {
            locationName = props.location.name;
        }
        if(locationAddress === "") {
            locationAddress = props.location.address;
        }
        if(cityId === 0){
            cityId = props.location.cityId;
        }
        location.name = locationName;
        location.address = locationAddress;
        location.cityId = cityId;
        props.onEditTestingLocation(location);
        setLocationName("");
        setLocationAddress("");
        setCityId(0);
        return props.onClose();
    }

    return (
        <div>
            <Dialog open={open} onClose={() => props.onClose()} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{props.title}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        
                    </DialogContentText>
                    <Select
                        defaultValue= {props.location.cityId}
                        onChange={handleChangeCity}
                        displayEmpty
                    >
                        <option>Chọn thành phố</option>
                        {cities.map(item => (
                            <option key = {item.id} 
                            value={item.id}>{item.name}</option>
                        ) )}
                    </Select>
                    <TextField
                        autoFocus
                        defaultValue = {props.location.name}
                        margin="dense"
                        id="name"
                        label="Tên bệnh viện"
                        type="text"
                        fullWidth
                        onChange = {(event) => handleChangeName(event)}
                    />
                    <TextField
                        autoFocus
                        defaultValue = {props.location.address}
                        margin="dense"
                        id="name"
                        label="Địa chỉ"
                        type="text"
                        fullWidth
                        onChange = {(event) => handleChangeAddress(event)}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.onClose()} color="primary">
                        Hủy
                    </Button>
                    <Button onClick={() => props.action ===0? handleSubmitAdd():handleSubmitEdit() } color="primary">
                        Lưu
                    </Button>
                    </DialogActions>
            </Dialog>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        data: state.getCityList.data,
        error: state.getCityList.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetCityList: () => dispatch(actions.getCity()),
        onEditTestingLocation: (location) => dispatch(actions.updateLocation(location)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditLocationDialog);
