import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import { useEffect } from 'react';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import classes from './EditCity.module.css'

function AddLocationDialog(props) {
    const [open, setOpen] = React.useState(false);
    let [locationName, setLocationName] = React.useState("");
    let [locationAddress, setLocationAddress] = React.useState("");
    let [location,setLocation] = React.useState({});
    let [cities, setCity] = React.useState([]);
    let [cityId, setCityId] = React.useState(0);
    let [errorCity, setErrorCity] = React.useState("");
    let [errorLocationName, setErrorLocationName] = React.useState("");
    let [errorLocationAddress, setErrorLocationAddress] = React.useState("");
  
    useEffect(() =>{
        setErrorCity("");
        setErrorLocationName("");
        setErrorLocationAddress("");
        setLocationName("");
        setLocationAddress("");
        setCityId(0);
        setOpen(props.open);
        props.onGetCityList();
        if(props.data){
            setCity(props.data);
        }
    },[props.location, props.open]);
    
    const handleChangeName = (event) => {
        const value = event.target.value;
        if(value !== ""){
            setLocationName(value);
            setErrorLocationName("");
        }else{
            setErrorLocationName("Vui lòng nhập tên địa điểm !");
        }
    }

    const handleChangeAddress = (event) => {
        const value = event.target.value;
        if(value !== ""){
            setLocationAddress(value);
            setErrorLocationAddress("");
        }else{
            setErrorLocationAddress("Vui lòng nhập địa chỉ!");
        }
    }
  
    const handleSubmitAdd = (event) => {
        if(locationName !== "" && locationAddress !== "" && cityId!==0){
            location.name = locationName;
            location.address = locationAddress;
            location.cityId = cityId;
            props.onAddTestingLocation(location);
            if(props.isSuccess){
                setLocationName("");
                setLocationAddress("");
                setCityId(0);
                props.onClose();
            }
        }
        if(locationAddress === ""){
            setErrorLocationAddress("Vui lòng nhập địa chỉ !");
        }
        if(locationName === ""){
            setErrorLocationName("Vui lòng nhập tên địa điểm !")
        }
        if(cityId === 0){
            setErrorCity("Vui lòng chọn thành phố");
        }

    }

    const handleChangeCity = (event)=> {
        const value = event.target.value;
        if(value !== 0){
            setCityId(value);
            setErrorCity("");
        }else {
            setErrorCity("Vui lòng chọn thành phố !");
        }   
    }

    return (
        <div>
            <Dialog open={open} onClose={() => props.onClose()} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Thêm địa điểm xét nghiệm</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        
                    </DialogContentText>
                    <Select
                        onChange={handleChangeCity}
                        displayEmpty
                    >
                        <option>Chọn thành phố</option>
                        {cities.map(item => (
                            <option key = {item.id} 
                            value={item.id}>{item.name}</option>
                        ) )}
                    </Select>
                    <p className = {classes.error}>{errorCity}</p>
                    
                    <TextField
                        autoFocus
                        margin="dense"
                        label="Tên bệnh viện"
                        type="text"
                        fullWidth
                        onChange = {(event) => handleChangeName(event)}
                    />
                    <p className = {classes.error}>{errorLocationName}</p>
                    
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Địa chỉ"
                        type="text"
                        fullWidth
                        onChange = {(event) => handleChangeAddress(event)}
                    />
                    <p className = {classes.error}>{errorLocationAddress}</p>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.onClose()} color="primary">
                        Hủy
                    </Button>
                    <Button onClick={() => handleSubmitAdd()} color="primary">
                        Lưu
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        data: state.getCityList.data,
        error: state.getCityList.error,
        isSuccess: state.addLocation.isSuccess
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetCityList: () => dispatch(actions.getCity()),
        onAddTestingLocation : (testingLocation) => dispatch(actions.addLocation(testingLocation))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddLocationDialog);
