import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import { useEffect } from 'react';
import * as actions from '../../store/actions/index';
import classes from './MedicalInfo.module.css'
import { connect } from 'react-redux';

function MedicalInfoDialog(props) {
    const [open, setOpen] = React.useState(false);
    let [testing,setTesting] = React.useState(props.testing);
    var medicalArr = [];

    medicalArr.push({
        key:   "Hen suyễn",
        value: testing.asthma === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Ho",
        value: testing.cough === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Sốt",
        value: testing.fever === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Bệnh tim",
        value: testing.heartProblem === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Cao huyết áp",
        value: testing.highBloodPressure === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "HIV",
        value: testing.hiv === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Béo phì",
        value: testing.obesity === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Mang thai",
        value: testing.pregnancy === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Sổ mũi",
        value: testing.runningNose === true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Khó thở",
        value: testing.shortnessOfBreath=== true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Mệt mỏi",
        value: testing.tiredness=== true? "Có": "Không"
    });
    medicalArr.push({
        key:   "Triệu chứng khác",
        value: testing.specialSymptom === "" ||null ?"Không": testing.specialSymptom
    });
    
    useEffect(() =>{
        setOpen(props.open);
        setTesting(props.testing);
    },[props.open]);
    const handleClose = () => {
        props.onClose();
    }
  
    return (
        <div >
            <Dialog  open={open} onClose={() => props.onClose()} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Thông tin khai báo y tế</DialogTitle>
                <DialogContent>
                    <DialogContentText className = {classes.dialog}>
                        <div  className = {classes.dialogContent}>
                            {medicalArr.slice(0,6).map((row)=> (
                                <p><strong>{row.key} : {row.value}</strong></p>
                            ))}
                        </div>
                        <div  className = {classes.dialogContent}>
                        {medicalArr.slice(6,11).map((row)=> (
                            <p><strong>{row.key} : {row.value}</strong></p>
                        ))}
                        </div>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.onClose()} color="primary">
                        Đóng
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default MedicalInfoDialog;
