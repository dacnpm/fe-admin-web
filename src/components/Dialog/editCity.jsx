import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import { useEffect } from 'react';
import * as actions from '../../store/actions/index';
import classes from './EditCity.module.css'
import { connect } from 'react-redux';

function EditCityDialog(props) {
    const [open, setOpen] = React.useState(false);
    let [cityName, setCityname] = React.useState(props.city.name);
    let [cityStatus, setCityStatus] = React.useState(props.city.status);
    let [errorName, setErrorName] = React.useState("");
    let [city,setCity] = React.useState(props.city);
    let cityList = props.cityList;
  
    useEffect(() =>{
        setOpen(props.open);
        setErrorName("");
        setCity(props.city);
        setCityname(props.city.name);
    },[props.open]);
    
    const handleChangeName = (event) => {
        const value = event.target.value;
        if(value !== ''){
            setCityname(value);
            setErrorName("");
        }else {
            setErrorName("Vui lòng nhập tên tỉnh/thành !");
        }
    }

    const handleChangeStatus = (event) => {
        const value = event.target.value;
        setCityStatus(value);
    }

    const checkExistName = (cityName) => {
        cityList = cityList.filter(row => row.id !== city.id);
        var cityExist = cityList.filter(row => row.name.toLowerCase().indexOf(cityName.toLowerCase()) !== -1);
        if(cityExist.length === 0){
            return false
        }else {
            return true;
        }
    }
     
    const handleSubmitEdit = (event) => {
        if(errorName === ""){
            if(checkExistName(cityName)){
                setErrorName("Tên tỉnh/thành đã tồn tại !")
            }else {
            var cityUpdate = {};
            cityUpdate.id = city.id;
            cityUpdate.name = cityName;
            cityUpdate.status = cityStatus;
            props.onEditCity(cityUpdate);
            if(props.isEditSuccess){
                props.onClose();
            }else {
                props.onClose();
            }
            }
        }else if(errorName !== "") {
            setErrorName("Vui lòng nhập tên tỉnh/thành !")
        }
    }

    return (
        <div>
            <Dialog open={open} onClose={() => props.onClose()} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Chỉnh sửa thông tin thành phố</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        
                    </DialogContentText>
                    <p className = {classes.error}>{errorName}</p>
                    <TextField
                        autoFocus
                        defaultValue = {props.city.name}
                        margin="dense"
                        id="name"
                        label="Tên thành phố"
                        type="email"
                        fullWidth
                        onChange = {(event) => handleChangeName(event)}
                    />
                    <Select
                        defaultValue = {props.city.status}
                        onChange={(event) => handleChangeStatus(event)}
                        >
                        <option value= {"Negative"}>An toàn</option>
                        <option value= {"Positive"}>Đang có dịch</option>
                    </Select>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.onClose()} color="primary">
                        Hủy
                    </Button>
                    <Button onClick={() =>handleSubmitEdit() } color="primary">
                        Lưu
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        data: state.addCity.data,
        isSuccess: state.addCity.isSuccess,
        error: state.addCity.error,
        isEditSuccess: state.updateCity.isSuccess
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddCity: (city) => dispatch(actions.addCity(city)),
        onEditCity: (city) => dispatch(actions.updateCity(city))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditCityDialog);
