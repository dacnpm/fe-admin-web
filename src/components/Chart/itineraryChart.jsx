import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import classes from './ItineraryChart.module.css';
import React from 'react';


export default function ItineraryChart(props){
    let result = props.result;
    let   series = []
   
    Object.entries(result).forEach(([key, value]) =>{
        series.push(
            {
                name: key,
                data: [value],
            }
        )
    })
    
    let options = {
        chart: {
            type: 'bar',
            width: 920,
            height: 400 
        },
        xAxis: {
            categories: [""],
            title: null,
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Số lượt (lượt)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Lượt'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'top',
            x: -10,
            y: 0,
            floating: true,
            borderWidth: 1,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
            shadow: true,
            width: 870,
        },
        credits: {
            enabled: false
        },
        series: series
    };

    return (
        <div id="atmospheric-composition" className = {classes.content}>
            <HighchartsReact
            highcharts={Highcharts}
            constructorType={'chart'}
            options={options}
            />
        </div>
    )
}

