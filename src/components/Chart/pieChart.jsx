import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official'
import classes from './PieChart.module.css';
import React, {useState, useEffect} from 'react'
import clsx from 'clsx';

export default function TestingResultChart(props){
    
    let [positivePercent, setPositive] = useState(props.positivePercent);
    let [negativePercent, setNegative] = useState(props.negativePercent);
    let [pendingPercent, setPending] = useState(props.pendingPercent);

    useEffect(() =>{
        setPositive(props.positivePercent);
        setNegative(props.negativePercent);
        setPending(props.pendingPercent);
    });
    console.log(positivePercent);

    let series = [{
        name: '%',
        data: [
            {
                name: 'Âm tính',
                y: positivePercent,
                color: '#2444F3'
            },
            {
                name: 'Dương tính',
                y:  negativePercent,
                color: '#F30026'
            },
            {
                name: 'Đang xử lý',
                y: pendingPercent,
                color: '#f4ce00'
            }
        ]
    }]
    //console.log(series);
    
    let  options = {
        chart: {
            type: 'pie',
            width: 270 ,
            height: 240,
            marginTop: 40,
        },
        title: {
            verticalAlign: 'top',
            floating: true,
            text: 'Biểu đồ kết quả XN',
            style: {
                fontSize: '13px',
            }
        },
        plotOptions: {
            pie: {
                dataLabels: "none",
                innerSize: '0%',
            },
        },
        series: series
    }
  
    return (
        <div id="atmospheric-composition" className = {classes.content}>
            <HighchartsReact
                highcharts={Highcharts}
                constructorType={'chart'}
                options={options}
            />
            <div className = "row">
                <div className = {clsx(classes.negativeCase,"col-1")}></div>
                <p className = "col-8">Dương tính</p>
            </div>
            <div className = "row">
                <div className = {clsx(classes.positiveCase,"col-1")}></div>
                <p className = "col-8"> Âm tính</p>
            </div>
            <div className = "row">
                <div className = {clsx(classes.pendingCase,"col-1")}> </div>
                <p className = "col-8">Đang xử lý</p>
            </div>
        </div>
    )
}

