import React ,{ Fragment }from 'react';
import UserImg from '../../assets/images/man.png';
import { Component } from 'react'
import {Avatar} from "@material-ui/core";

export default function UserAvatar() {  
    return (
        <div>
            <Avatar alt="Remy Sharp" src={UserImg}/>
        </div>
    );
  }