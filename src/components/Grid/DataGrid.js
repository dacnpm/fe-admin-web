import { DataGrid } from '@material-ui/data-grid'; 
import { makeStyles } from "@material-ui/core/styles";

const styles = {
    overrides: {
    MuiDataGridColumnsContainer: {
        padding: "0 15px !important"
    }
    }
  };
  
  const useStyles = makeStyles(styles);
  
  export default function GridItem(props) {
    const classes = useStyles();
    const { rows, columns, ...rest } = props;
    return (
        <DataGrid item {...rest} className={classes.MuiDataGridColumnsContainer}>
        </DataGrid>
    );
  }