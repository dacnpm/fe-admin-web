import { Fragment } from 'react';
import {classes} from './DashboardBox.module.css';

const DashboardBox = (props) => {
    var class_name = "icon " + props.classes;
    return ( 
        <Fragment>
            <div className = {classes.dashboardBox}>
                <div className = {class_name}>
                    {props.icon}
                </div>
                <div className = "title">
                    <p>{props.title}</p>
                </div>
                <div className = "information">
                    <p>{props.info}</p>
                </div>
            </div>
        </Fragment> 
    );
}
 
export default DashboardBox;