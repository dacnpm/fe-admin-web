import PropTypes from "prop-types";
import React, {useState} from "react";
import { makeStyles, useTheme} from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';


const useStyles1 = makeStyles((theme) => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
    inputPage: {
        width: 37,
        marginLeft: 6,
        // color: '#2546FF',
        fontWeight: 400,
    },
}));


function TextMaskCustom(props) {
    const { inputRef, ...other } = props;
  
    return (
      <MaskedInput
        {...other}
        ref={(ref) => {
          inputRef(ref ? ref.inputElement : null);
        }}
        mask={[/[1-9]/, /[1-9]/]}
        placeholderChar={'\u2000'}
        showMask
      />
    );
}

TextMaskCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
};


export default function TablePaginationActions(props) {
    const classes = useStyles1();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage} = props;

    const MAX_VAL = Math.ceil(count / rowsPerPage);
    const withValueCap = (inputObj) => {
        let  {value}  = inputObj;
        console.log(value);
        return (value !== '0' && value <= MAX_VAL) ?  true : false;
      };
    let [inputPage, setInputPage] = useState(page+1);

    const handleFirstPageButtonClick = (event) => {
        onChangePage(event, 0);
        setInputPage(1);
    };

    const handleBackButtonClick = (event) => {
        onChangePage(event, page-1);
        setInputPage(page);
    };

    const handleNextButtonClick = (event) => {
        onChangePage(event, page + 1);
        setInputPage(page + 2);
    };

    const handleLastPageButtonClick = (event) => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage)-1));
        setInputPage(Math.ceil(count / rowsPerPage));
    };
    const handleChangePageNumber = (event) => {
        var number = event.target.value;
        if(number > 0){
            setInputPage(number);
            onChangePage(event, number-1);
        }else {
            setInputPage();
        }
    };
    // const checkNumber = (event) => {
    //     var number = event.target.value;
    //     console.log(number);
    //     if(number <= 0 ){
    //         setInputPage();
    //     }else {
    //         setInputPage();
    //     }
    // };
    return (
        <div className={classes.root}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
                title = "Trang đầu tiên"
            >
                {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton
                onClick={handleBackButtonClick}
                disabled={page === 0}
                aria-label="previous page"
                title = "Trang trước"
            >
                {theme.direction === "rtl" ? (
                <KeyboardArrowRight />
                ) : (
                <KeyboardArrowLeft />
                )}
            </IconButton>
            Trang <NumberFormat 
            allowNegative ={false}
            displayType = 'input'
            onChange= {(event) => handleChangePageNumber(event)}
            className = {classes.inputPage}
            value = {inputPage}
            decimalScale = {0}
            isAllowed={withValueCap} 
            /> / {Math.ceil(count / rowsPerPage)}
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
                title = "Trang tiếp theo"
            >
                {theme.direction === "rtl" ? (
                <KeyboardArrowLeft />
                ) : (
                <KeyboardArrowRight />
                )}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
                title = "Trang cuối"
            >
                {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </div>
    );
}

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};
