import React  from 'react';
import Layout from '../../hoc/Layout/Layout';
import classes from './notFound.module.css';

const NotFound = () => {
    return ( 
        <Layout>
            <div className={classes.mainContain}>
                <h3>Không tìm thấy trang, địa chỉ không tồn tại !</h3>
            </div>
        </Layout>
    );
}
 
export default NotFound;