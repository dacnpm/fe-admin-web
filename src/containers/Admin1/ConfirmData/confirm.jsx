import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';// react plugin for creating charts
import { TextField} from '@material-ui/core';
// @material-ui/icons
import {Grid, Paper, Typography, Box} from "@material-ui/core";
import {Button} from "@material-ui/core";
import Layout from "../../../hoc/Layout/Layout";
import React, {useState } from 'react'
import classes from './confirm.module.css';
import QrReader from 'react-qr-scanner'
import {dateTimeFormat} from '../../../store/utility'

function ConfirmData(props){
    let [email, setEmail] = useState("");
    let [display, setDisplay]   = useState("none");
    let [open, setOpen]  = useState(false);

    const inputChangedHandler = (event) => {
        var value = event.target.value;
        setEmail(value);
    }

    const submitHandler = (event) => {
        props.onConfirm(email);
        setDisplay("block")
    }

    const handleScan = (data) =>{
        if(data){
            console.log(data);
            var arr  = data.text.split('"');
            props.onConfirm(arr[3]);
            setOpen(false);
            setDisplay("block")
        }
    }

    const handleError = (err)=>{
        console.error(err)
    }

    const previewStyle = {
        height: 300,
        width: 280,
    }

    var content = "";
    if(props.isSuccess){
        content = <table>
                    <tr>
                        <th>Email:</th>
                        <td>{props.email}</td>
                    </tr>
                    <tr>
                        <th>Nơi khởi hành:</th>
                        <td>{props.departure}</td>
                    </tr>
                    <tr>
                        <th>Nơi đến:</th>
                        <td>{props.destination}</td>
                    </tr>
                    <tr>
                        <th>Giờ khởi hành:</th>
                        <td>{dateTimeFormat(props.departureTime,  "ddd  dd/mmm/yyyy HH:MM:ss TT")}</td>
                    </tr>
                    <tr>
                        <th>Giờ tới: </th>
                        <td>{dateTimeFormat(props.landingTime,  "ddd  dd/mmm/yyyy HH:MM:ss TT")}</td>
                    </tr>
                    <tr>
                        <th>Số hiệu phương tiện: </th>
                        <td className={classes.healthCondition}>{props.travelNo}</td>
                    </tr>
                </table>
        }else{
            content = <p className={classes.errorMessage}>{props.error}</p>
        }
        return (
            <div>
                <Layout>
                    {/* <Dialog open={open} onClose={() => handleClose} aria-labelledby="form-dialog-title">
                        <DialogTitle   DialogTitle id="form-dialog-title">Scan QR Code</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                            
                            </DialogContentText> */}
                        {/* </DialogContent>
                        <DialogActions>
                            <Button onClick={() => handleClose()} color="primary">
                                Hủy
                            </Button>
                        </DialogActions>
                    </Dialog> */}
                    <div className={classes.mainContain}>
                        <div className = {classes.title}>
                            <h1 className={classes.titleText}>Xác nhận thông tin khai báo</h1>
                        </div>
                        <div className = {classes.formCode}>
                            {/* <Button variant="contained" color="primary" 
                                className={classes.scanQR}
                                onClick = {() => handleOpenScanQR()}
                            >
                                Quét mã QR
                            </Button> */}
                     
                                <TextField className = {classes.textField}
                                    variant ="outlined"
                                    label="Nhập vào email"
                                    size = "small"
                                    onChange={(event) => inputChangedHandler(event)}/>
                                <Button variant="contained" color="primary" 
                                    size = "small"
                                    className={classes.buttonSubmit}
                                    onClick = {()=> submitHandler()}>
                                    OK
                                </Button>
                       
                        </div>
                        <div className = {classes.confirmData}>
                        <QrReader
                                className = {classes.scanQR}
                                style={previewStyle}
                                onError={() => handleError()}
                                onScan={(data) => handleScan(data)}
                                />
                            <Box maxWidth="xm" className={classes.container}display={display}>
                                <Typography component="div" style={{ backgroundColor: '#fff', height: '55vh' }}>
                                    <Paper className = {classes.infoTitle}>
                                        THÔNG TIN ĐÃ KHAI BÁO
                                    </Paper>
                                    <div className={classes.information}>
                                        <Grid container spacing={3}>
                                            <Grid item xs={3}>
                                                {/* <UserAvatar/> */}
                                            </Grid>
                                            <Grid item xs={6}>
                                               {content}
                                            </Grid>
                                        </Grid>
                                    </div>
                                </Typography>
                            </Box>
                        </div>
                    </div>
                </Layout>
            </div>
        )
}

const mapStateToProps = state => {
    return {
        isSuccess: state.confirm.isSuccess,
        email: state.confirm.email,
        departure: state.confirm.departure,
        destination: state.confirm.destination,
        departureTime: state.confirm.departureTime,
        landingTime: state.confirm.landingTime,
        travelNo: state.confirm.travelNo,
        error: state.confirm.error
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      onConfirm: (email) => dispatch(actions.confirm(email)),
      onCloseModalErrorConfirm: () => dispatch(actions.closeModalErrorConfirm())
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(ConfirmData);
