import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Store from "@material-ui/icons/Store";
import PeopleIcon from '@material-ui/icons/People';
import DateRange from "@material-ui/icons/DateRange";
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader";
import CardIcon from "../../components/Card/CardIcon";
import CardFooter from "../../components/Card/CardFooter";
import {useEffect, useState} from 'react';
import styles from "../../assets/jss/dashboardStyle.js";
import Layout from "../../hoc/Layout/Layout.jsx";
import customStyle  from './dashboard.module.css';

const useStyles = makeStyles(styles);

function Dashboard(props) {
    const classes = useStyles();
    let [totalUser, setTotalUser] = useState(0);
    let [negativeCases, setNegativeCase] = useState(0);
    let [positiveCases, setPositiveCase] = useState(0);
    let [users, setUser] = useState([]);
    let [testings, setTesting] = useState([]);

    useEffect(() => {
        if(!users.length && !testings.length){
            props.onGetTesting();
            props.onGetUsers();
            if (props.users) {
                setUser(props.users)
                setTotalUser(props.users.length);
            }
            if(props.testings){
                setTesting(props.testings)
                setNegativeCase(props.testings.filter(test => test.testingResultDto.result == "Negative").length);
                setPositiveCase(props.testings.filter(test => test.testingResultDto.result == "Positive").length)
            }
        }
    },[props.users]);

    const StyledGridContainer = withStyles((theme) => ({
        root: {
            '& .MuiGrid-root': {
            marginLeft: '60px'
            },
        },
    }))(GridContainer);

    return (
        <div>
            <Layout history = {props.history}>
                <StyledGridContainer className = {customStyle.container}>
                    <GridItem xs={12} sm={6} md={3}>
                        <Card>
                            <CardHeader color="warning" stats icon>
                            <CardIcon color="warning">
                                <PeopleIcon />
                            </CardIcon>
                            <p className={classes.cardCategory}>Tổng người dùng</p>
                            <h3 className={classes.cardTitle}>
                                {totalUser}
                            </h3>
                            </CardHeader>
                            <CardFooter stats>
                            <div className={classes.stats}>
                                Người dùng
                            </div>
                            </CardFooter>
                        </Card>
                    </GridItem>
                    <GridItem xs={12} sm={6} md={3}>
                        <Card>
                            <CardHeader color="success" stats icon>
                                <CardIcon color="success">
                                    <Store />
                                </CardIcon>
                                <p className={classes.cardCategory}>Âm tính</p>
                                <h3 className={classes.cardTitle}>{positiveCases}</h3>
                            </CardHeader>
                            <CardFooter stats>
                                <div className={classes.stats}>
                                    <DateRange />
                                    Kết quả XN
                                </div>
                            </CardFooter>
                        </Card>
                    </GridItem>
                    <GridItem xs={12} sm={6} md={3}>
                        <Card>
                            <CardHeader color="danger" stats icon>
                                <CardIcon color="danger">
                                    <Store />
                                </CardIcon>
                                <p className={classes.cardCategory}>Dương tính</p>
                                <h3 className={classes.cardTitle}>{negativeCases}</h3>
                            </CardHeader>
                            <CardFooter stats>
                                <div className={classes.stats}>
                                <DateRange />
                                    Kết quả XN
                                </div>
                            </CardFooter>
                        </Card>
                    </GridItem>
                </StyledGridContainer>
            </Layout>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        testings: state.getTesting.data,
        error: state.getTesting.error,
        users: state.getUsers.data,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetUsers: () => dispatch(actions.getUsers()),
        onGetTesting: () => dispatch(actions.getTesting()),
        
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
