import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import classes from './login.module.css';
import Input from './../../components/UI/Input/Input';
import Button from './../../components/UI/Button/Button';

class Login extends Component {
    state = {
        errorMessage: "",
        controls: {
            email: {
                elementName: 'Email',
                elementType: 'input',
                elementConfig: {
                type: 'email',
                er: 'Vui lòng nhập đúng định dạng Email',
                placeholder:'Nhập email của bạn'
                },
                value: '',
                validation: {
                required: true,
                isEmail: true
                },
                valid: true,
                touched: true
            },
            password: {
                elementName: 'Mật khẩu',
                elementType: 'input',
                elementConfig: {
                type: 'password',
                placeholder: 'Nhập mật khẩu của bạn'
                },
                value: '',
                validation: {
                required: true,
                },
                valid: true,
                touched: true
            }
        },
        passwordShown: false
    };

    togglePasswordVisiblity = () => {
        this.setState(prevState => {
            return { passwordShown: !prevState.passwordShown }
        });
    };

    checkValidity = (value, rules) => {
        let isValid = true;

        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid;
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid;
        }

        return isValid;
    };

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({ controls: updatedControls });
    }

    submitHandler = (event) => {
        if(this.state.controls.email.valid) {
            event.preventDefault();
            this.props.onLogin(this.state.controls.email.value, this.state.controls.password.value);
            setTimeout(() => {
                if (localStorage.getItem('user') !== null) {
                    window.location = "/admin/dashboard"
                }else {
                    this.setState({errorMessage: this.props.error});
                    console.log(this.props);
                }
            }, 1000);
            // setTimeout(() => {
            //     if (this.props.== 404) {
            //         console.log("Không lấy được data");
            //     }
            // }, 4000);
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => {
            let input = <Input
                className={classes.inputField}
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                valueType={formElement.id}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
            />;

            if (formElement.config.elementConfig.placeholder === 'Password') {
                input = (
                    <div className={classes.PasswordFeild}>
                        <Input
                            className={classes.inputField}
                            key={formElement.id}
                            elementType={formElement.config.elementType}
                            elementConfig={formElement.config.elementConfig}
                            value={formElement.config.value}
                            invalid={!formElement.config.valid}
                            shouldValidate={formElement.config.validation}
                            touched={formElement.config.touched}
                            valueType={formElement.id}
                            showPassword={this.state.passwordShown}
                            changed={(event) => this.inputChangedHandler(event, formElement.id)}
                        />
                    </div>
                );
            }

            return (
                <div className={classes.InputElement} key = {formElement.config.elementName}>
                <h5 className={classes.InputType}>{formElement.config.elementName + "*"}</h5>
                {input}
                </div>
            );
        });

        return (
            <div className={classes.LoginContainer}>
                <div className={classes.Login}>
                    <h3>ĐĂNG NHẬP</h3>
                    <div className={classes.errorMessage}>
                        <p>{this.state.errorMessage}</p>
                    </div>
                    <form>
                        {form}
                    </form>
                    <Button  className = {classes.submitButton} clicked={this.submitHandler}>Đăng nhập</Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isSuccess: state.login.isSuccess,
        userId: state.login.userId,
        token: state.login.token,
        fullName: state.login.fullName,
        role: state.login.role,
        error: state.login.error,
        loading: state.login.loading,
        errorCode: state.login.errorCode
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (email, password) => dispatch(actions.login(email, password)),
        onCloseModalErrorLogin: () => dispatch(actions.closeModalErrorLogin())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);