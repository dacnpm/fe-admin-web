import React from 'react';
import './sidebar.css';
import SideBarItem1 from './SideBarAdmin1/SideBarItem1';
import SideBarItem2 from './SideBarAdmin2/SideBarItem2';


export default function SideBar(props) {
    const handleLogout = () => {
        localStorage.clear();
        window.location = "/login"
    };
    if(localStorage.getItem('token') != null){
        const role = localStorage.getItem('roles');
        const fullName = localStorage.getItem('user');
        var sideBar = '';
        if(role === "ADMIN1"){
            sideBar =  <SideBarItem1 fullName = {fullName} onLogout = {handleLogout}/>;
        }
        if( role === "ADMIN2"){
            sideBar =  <SideBarItem2 onLogout = {handleLogout} fullName = {fullName} />;
        }
    }
    return ( 
        <nav id="sidebar">     
            {sideBar}
        </nav>
    );
}
