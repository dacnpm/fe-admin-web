import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ConfirmationNumberIcon from '@material-ui/icons/ConfirmationNumber';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import FilterListIcon from '@material-ui/icons/FilterList';

const drawerWidth = 290;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        background: '#fff',
        color: '#000',
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        background: '#06041f',
        color: '#fff',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        background: '#06041f',
        color: '#fff',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    avatarClose : {
        cursor: "pointer",
        marginLeft: '970px',
        width: '50px',
        height:'50px'
    },
    avatarOpen : {
        cursor: "pointer",
        marginLeft: '870px',
        width: '50px',
        height:'50px'
    },
    sidebarItem :{
        color: '#a4acb7',
        fontSize: '12px !important',
        fontFamily: 'Arial !important'
    },
    menuIcon : {
        color: "#fff"
    },
    userAction : {
        position: "relative !important",
        display: "none"
    }
}));

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:focus': {
             backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },
}))(MenuItem);

const StyledSidebarItem= withStyles((theme) => ({
    root: {
        '& .MuiListItemText-root, & .MuiTypography-body1': {
            fontSize: '0.87rem !important',
            color: '#a4acb7 !important',
            fontFamily: 'Arial !important'
        },
    },
}))(ListItemText);

export default function SideBarItem1(props) {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                        [classes.hide]: open, 
                        })}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" >
                        CovidAdmin
                    </Typography>
                    <div>
                        <AccountCircle className={open==true? classes.avatarOpen: classes.avatarClose}  onClick={handleClick}/>
                        <StyledMenu
                            id="customized-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleClose}>
                            <StyledMenuItem onClick = {props.onLogout}>
                                <ListItemIcon>
                                    <ExitToAppIcon fontSize="small" />
                                </ListItemIcon>
                                    <ListItemText primary="Đăng xuất" />
                            </StyledMenuItem>
                        </StyledMenu>
                    </div>
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
                })}
                classes={{
                paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                }),
                }}
            >
                <div className={classes.toolbar}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon color = "primary" fontSize = "large"/> 
                        : <ChevronLeftIcon color = "primary" fontSize = "large"/>}
                    </IconButton>
                </div>
                <Divider />
                    <List>
                        <Link to='/admin/dashboard' title = "Trang chủ">
                            <ListItem button>
                                <ListItemIcon className={classes.menuIcon}><DashboardIcon /></ListItemIcon>
                                <StyledSidebarItem primary= "Trang chủ" className = {classes.sidebarItem}/>
                            </ListItem>
                        </Link>
                        <Link to='/admin/confirm-data' title = "Xác nhận khai báo lịch trình">
                            <ListItem button>
                                <ListItemIcon className={classes.menuIcon} > <ConfirmationNumberIcon/></ListItemIcon>
                                <StyledSidebarItem primary= "Xác nhận khai báo" className = {classes.sidebarItem}/>
                            </ListItem>
                        </Link>
                        <Link to='/admin/itinerary/statistic' title = "Thống kê lịch trình">
                            <ListItem button>
                                <ListItemIcon  className={classes.menuIcon}> <FilterListIcon/></ListItemIcon>
                                <StyledSidebarItem primary= "Thống kê lịch trình" className = {classes.sidebarItem}/>
                            </ListItem>
                        </Link>
                    </List>
                <Divider />
            </Drawer>
        </div>
    );
}
