import React, { Fragment } from 'react';
import './header.css';
import { ToggleMenu } from './../../components/Icon/menu';
import UserAvatar from './../../components/UserAvatar/UserAvatar';

const Header = (props) => {
    return ( 
        <Fragment>
           <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className = "toggle_menu" >
                            <ToggleMenu/>
                        </div>
                        <UserAvatar/>
                    </div>
            </nav>
        </Fragment>
    );
}
 
export default Header;