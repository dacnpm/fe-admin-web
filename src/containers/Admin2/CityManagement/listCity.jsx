import React, {useEffect,useState} from 'react';
import { makeStyles, withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Layout from './../../../hoc/Layout/Layout';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import EditCityDialog from '../../../components/Dialog/EditCity';
import AddCityDialog from '../../../components/Dialog/AddCity';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import { TextField} from '@material-ui/core';
import TableHead from "@material-ui/core/TableHead";
import {Button} from "@material-ui/core"; 
import TablePaginationActions from '../../../components/Pagination/TablePagination';
import DeleteIcon from '@material-ui/icons/Delete';

const StyledTablePagination = withStyles((theme) => ({
    root: {
        '& .MuiTablePagination-toolbar': {
            position: 'absolute !important'
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 198,
        maxWidth: 700,
    },
    paper: {
        marginLeft: 170,
        width: 1100,
        overflow: 'hidden'
    },
    title: {
        marginBottom: 20,
        marginTop: 10,
        fontFamily: "Arial !important",
        fontWeight: "bold",
        fontSize: 16,
        textAlign: "center",
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableRow : {
        borderLeft: 2
    },
    tableFooter : {
        marginBottom: 200,
        marginLeft: 100
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14
    },
    positiveResult : {
        color: "green",
        fontWeight: "bold",
        fontSize: 14
    },
    IconButton : {
        cursor: "pointer",
        marginLeft: 10
    },
    textField: {
        width: 240,
        marginLeft: 197,
        marginRight: 260,
        marginBottom: 10,
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function ListCity(props) {
    let [rows, setData] = useState([]);
    let [city, setCity] = useState({});
    let [open, setOpen] = useState(false)
    let [openAdd, setOpenAdd] = useState(false)
    let [title , setTitle] = useState("");
    let [action , setAction] = useState();
    let [searchInput, setSearchInput] = React.useState("");

    useEffect(() =>{
        if(!rows.length)
        {
            props.onGetCityList();
            if(props.data){
                setData(props.data);
            }
        }
    },[props.data]);

    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleOpenDialog = (city, title, action) => {
        setOpen(true);
        setCity(city);
        setTitle(title);
        setAction(action);
    }

    const handleOpenAddDialog = () => {
        setOpenAdd(true);
    }

    const handleClose = ()=> {
        setOpen(false);
    }
    
    const handleCloseAdd = ()=> {
        setOpenAdd(false);
    }
   
    const inputChangedSearchHandler = (event) => {
        var value = event.target.value;
        setSearchInput(value);
    }

    // const handleDeleteCity = (cityId) => {
    //     actions.deleteCity(cityId);
    // }

    if(rows !== []){
        rows = rows.sort((a, b) => (a.id < b.id? 1 : -1));
    }
    if(searchInput !== ""){
        rows = rows.filter(row => row.name.toLowerCase().indexOf(searchInput.toLowerCase()) !== -1);
    }

    return (
        <Layout>
            <EditCityDialog action ={action} title = {title} 
                open = {open} city = {city}
                cityList = {rows}
                onClose = {()=> handleClose()}/>
            <AddCityDialog  
                open = {openAdd} city = {city}
                cityList = {rows}
                onClose = {()=> handleCloseAdd()}/>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h5>QUẢN LÝ THÔNG TIN THÀNH PHỐ</h5>
                </div>
                <TextField
                    id="outlined-secondary"
                    label="Tìm theo tên thành phố"
                    variant="outlined"
                    size = "small"
                    color="primary"
                    onChange={(event) => inputChangedSearchHandler(event)}
                    className = {classes.textField}
                />
                <Button variant="contained" color="primary" 
                    onClick = {() => handleOpenAddDialog()} >
                    Thêm tỉnh/thành phố
                </Button>
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow className={classes.tableRow}>
                            <StyledTableCell style={{ width: 180 }} align="left" className={classes.thumbnail}>
                                Tên tỉnh/thành
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 180 }} align="left" className={classes.thumbnail}>
                                Tình trạng
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 180}} align="left" className={classes.thumbnail}>
                                Tác vụ
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                            ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            : rows
                        ).map((row) => (
                        <StyledTableRow key={row.id} className={classes.tableRow}>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                {row.name}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                <p className = {row.status === "Positive"? classes.negativeResult : classes.positiveResult}>
                                    {row.status === "Positive"? "Đang có dịch" : "An toàn"}
                                </p>
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                <BorderColorIcon className = {classes.IconButton} 
                                    onClick = {(city, title, action) => handleOpenDialog(row, "Chỉnh sửa thông tin", 1)}>
                                </BorderColorIcon>
                                {/* <DeleteIcon className = {classes.IconButton}
                                    onClick={(cityId) => {
                                        const confirmBox = window.confirm("Bạn có chắc muốn xóa thành phố này ?")
                                        if (confirmBox === true) {
                                            handleDeleteCity(row.id)
                                        }
                                    }}>
                                </DeleteIcon> */}
                            </StyledTableCell>
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <StyledTableRow  >
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: rows.length }]}
                                colSpan={3}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { 'aria-label': 'rows per page' },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </StyledTableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = state => {
    return {
        data: state.getCityList.data,
        error: state.getCityList.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetCityList: () => dispatch(actions.getCity()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListCity);
