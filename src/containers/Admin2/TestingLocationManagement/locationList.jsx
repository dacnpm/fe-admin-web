import React, {useEffect,useState} from 'react';
import { makeStyles,withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Layout from './../../../hoc/Layout/Layout';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import Select from '@material-ui/core/Select';
import EditLocationDialog from '../../../components/Dialog/EditTestingLocation';
import AddLocationDialog from '../../../components/Dialog/AddTestingLocation';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import { TextField} from '@material-ui/core';
import TableHead from "@material-ui/core/TableHead";
import {Button} from "@material-ui/core";
import TablePaginationActions from '../../../components/Pagination/TablePagination';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const StyledTablePagination = withStyles((theme) => ({
    root: {
        '& .MuiTablePagination-toolbar': {
            position: 'absolute !important'
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 97,
        maxWidth: 900,
    },
    paper: {
        marginLeft: 140,
        width: 1100,
        overflow: 'hidden'
    },
    title: {
        marginBottom: 10,
        fontFamily: "Arial !important",
        fontSize: 17,
        textAlign: "center",
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableRow : {
        borderLeft: 2
    },
    tableFooter : {
        marginBottom: 200,
        marginLeft: 100
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14
    },
    positiveResult : {
        color: "green",
        fontWeight: "bold",
        fontSize: 14
    },
    IconButton : {
        cursor: "pointer",
        marginLeft: 10
    },
    textField: {
        width: 240,
        marginLeft: 30,
        marginRight: 170,
        marginBottom: 10,
    },
    selectedField : {
        marginTop: 7
    },
    allSelect : {
        marginLeft: 100,
        marginRight: 20,
        marginTop: 7,
        float: 'left'
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function TestingLocationList(props) {
    let [cityId, setSelectCity]  = useState(0);
    let [rows, setData] = useState([]);
    let [cityList, setCity] = useState([]);
    let [location, setLocation] = useState({});
    let [open, setOpen] = useState(false);
    let [openAdd, setOpenAdd] = useState(false)
    let [title , setTitle] = useState("");
    let [action , setAction] = useState();
    let [searchInput, setSearchInput] = React.useState("");
    let [selectedValue, setSelectedValue] = React.useState("0");
    var locations = [];

    useEffect(() =>{
        props.onGetTestingLocation();
        if(props.data){
            setData(props.data);
        }
        props.onGetCityList();
        if(props.city){
            setCity(props.city);
        }
    },[props.data]);

    locations = rows ;
    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleOpenDialog = (location, title, action) => {
        setOpen(true);
        setLocation(location);
        setTitle(title);
        setAction(action);
    }

    const handleOpenAddDialog = () => {
        setOpenAdd(true);
    }

    const handleClose = ()=> {
        setOpen(false);
    }
    
    const handleCloseAdd = ()=> {
        setOpenAdd(false);
    }
    
    const handleSelectCity = (event) => {
        var value = event.target.value;
        setSelectCity(value);
        setSelectedValue("");
    }

    const inputChangedSearchHandler = (event) => {
        var value = event.target.value;
        setSearchInput(value);
        setSelectedValue("");
    }

    const handleSelectAll = (event) => {
        setSelectedValue(event.target.value);
        setSelectCity(0);
        setSearchInput("");
    };

    // const handleDeleteCity = (locationId) => {
    //     actions.deleteLocation(locationId);
    // }

    if(cityId !== 0){
        rows = rows.filter(row => row.cityId == cityId);
    }
    rows = rows.filter(row => row.name.toLowerCase().indexOf(searchInput.toLowerCase()) !== -1);

    if(selectedValue === "0"){
        rows = locations;
    }


    return (
        <Layout>
            <EditLocationDialog action ={action} title = {title} open = {open}
                locations = {rows}
                location = {location} onClose = {()=> handleClose()}/>
            <AddLocationDialog open = {openAdd} 
                locations = {rows}
                onClose = {()=> handleCloseAdd()}/>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h4>Quản lý địa điểm xét nghiệm</h4>
                </div>
                <RadioGroup row aria-label="sort"  className={classes.allSelect}
                    name="sort1" value={selectedValue} onChange={(event) =>handleSelectAll(event)} onClick = {()=> setSelectedValue("")}>
                    <FormControlLabel value="0" control={<Radio color= "primary"/>} label="Tất cả" />
                </RadioGroup>
                <Select
                    className = {classes.selectedField}
                    native
                    value = {cityId}
                    onChange={(event) => handleSelectCity(event)}
                    >
                    <option value={0}>Chọn Thành phố</option>
                    {cityList.map(city =>  (
                        <option key= {city.id} value = {city.id}>{city.name}</option>
                    ) 
                    )}
                </Select>
                <TextField
                    id="outlined-secondary"
                    label="Tìm theo tên địa điểm"
                    variant="outlined"
                    size = "small"
                    color="primary"
                    value = {searchInput}
                    onChange={(event) => inputChangedSearchHandler(event)}
                    className = {classes.textField}
                />
                <Button variant="contained" color="primary" 
                    onClick = {() => handleOpenAddDialog()}>
                    Thêm địa điểm
                </Button>
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow className={classes.tableRow}>
                            <StyledTableCell style={{ width: 230 }} align="left" className={classes.thumbnail}>
                                Tên bệnh viện
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left" className={classes.thumbnail}>
                                Thành phố
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 300 }} align="left" className={classes.thumbnail}>
                                Địa chỉ
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 140}} align="left" className={classes.thumbnail}>
                                Tác vụ
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                        ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rows).map((row) => 
                        ( 
                        <StyledTableRow key={row.id} className={classes.tableRow}>
                            <StyledTableCell style={{ width: 230 }} align="left">
                                {row.name}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left" className={classes.thumbnail}>
                                {row.city.name}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 300 }} align="left">
                                {row.address}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 140 }} align="left">
                                <BorderColorIcon className = {classes.IconButton} 
                                    onClick = {(location, title, action) => handleOpenDialog(row, "Chỉnh sửa thông tin địa điểm", 1)}>
                                </BorderColorIcon>
                                {/* <DeleteIcon className = {classes.IconButton}
                                    onClick={(cityId) => {
                                        const confirmBox = window.confirm("Bạn có chắc muốn xóa thành phố này ?")
                                        if (confirmBox === true) {
                                            handleDeleteCity(row.id)
                                        }
                                    }}>
                                </DeleteIcon> */}
                            </StyledTableCell>
                        </StyledTableRow>
                        ))}

                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <TableRow  >
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: rows.length }]}
                                colSpan={3}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { 'aria-label': 'rows per page' },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = state => {
    return {
        data: state.getLocation.data,
        error: state.getLocation.error,
        city: state.getCityList.data
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetTestingLocation: () => dispatch(actions.getLocation()),
        onGetCityList : () => dispatch(actions.getCity()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TestingLocationList);
