import React, { useEffect,useState} from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Layout from "../../../hoc/Layout/Layout";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import { Link } from "react-router-dom";
import { dateTimeFormat } from "../../../store/utility";
import { TextField } from "@material-ui/core";
import TableHead from "@material-ui/core/TableHead";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TablePaginationActions from '../../../components/Pagination/TablePagination';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import clsx from 'clsx';
import {now} from 'moment';
import {Button} from "@material-ui/core"; 
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const StyledTablePagination = withStyles((theme) => ({
    root: {
        "& .MuiTablePagination-toolbar": {
             position: "absolute !important",
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 50,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 180,
        width: 1100,
        overflow: 'hidden'
    },
    textField: {
        width: 210,
        marginBottom: 10,
        float: "left"
    },
    title: {
        marginTop: 10,
        fontFamily: "Arial !important",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 17,
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14,
    },
    positiveResult: {
        color: "green",
        fontWeight: "bold",
        fontSize: 14,
    },
    textFieldDate: {
        width: 170,
        marginLeft: 10,
        marginBottom: 10,
    },
    lableField : {
        fontSize: 15,
        marginLeft: 5,
        marginTop: 10,
        float: 'left'
    },
    allSelect : {
        marginLeft: 70,
        marginRight: 30,
        float: 'left'
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const StyledTableHead = withStyles((theme) => ({
    root: {
        "& .TableCell-head-35": {
            backgroundColor: "#132169 !important"
        },
    },
}))(TableHead);

function PostManagement(props) {
    let [rows, setData] = useState([]);
    let [postDate, setPostDate] = useState("");
    let [postTitle, setPostTitle] = React.useState("");
    let [postId, setPostId] = React.useState();
    const [open, setOpen] = React.useState(false);
    let [selectedValue, setSelectedValue] = React.useState("0");
    var posts = [];

    useEffect(() => {
        if(!rows.length)
        {
            props.onGetNews();
            if (props.data) {
                setData(props.data);
            }
        }
    },[props.data, props]);

    posts = rows.sort((a, b) => (a.id < b.id? 1 : -1));
    const classes = useStyles2();
    const [page, setPage] = React.useState(props.location.oldPage != undefined ? props.location.oldPage: 0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const inputChangedSearchHandler = (event) => {
        var value = event.target.value;
        setPostTitle(value);
        setSelectedValue("");
    };

    const handleSelectPostingDate = (event) => {
        setPostDate(event.target.value);
        setSelectedValue("");
    };
    
    if(postTitle !== ""){
        rows = rows.filter(news => news.title.toLowerCase().indexOf(postTitle.toLowerCase()) !== -1);
    }
    if(postDate !== ""){
        rows = rows.filter(
            (row) => dateTimeFormat(row.createdAt, "dd/mmm/yyyy") === dateTimeFormat(postDate, "dd/mmm/yyyy")
        );
    }
    const handleClose = () => {
        setOpen(false);
      };

    const handleDeleteNews = (newsId) => {
        setOpen(true);
        setPostId(newsId);
    }

    const handleClickDelete = (event) => {
        props.onDeleteNews(postId)
        handleClose();
    }

    const handleSelectAll = (event) => {
        setSelectedValue(event.target.value);
        setPostDate("");
        setPostTitle("");
    };

    if(rows !== []){
        rows = rows.sort((a, b) => (a.id < b.id? 1 : -1));
    }

    if(selectedValue === "0"){
        rows = posts;
    }


    return (
        <Layout>
            <Dialog
                open={open}
                aria-labelledby="draggable-dialog-title"
            >
                <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                    Xóa bảng tin
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Bạn có chắc muốn xóa tin này ?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        HỦY
                    </Button>
                    <Button onClick={() => handleClickDelete()} color="primary">
                        XÓA
                    </Button>
                </DialogActions>
            </Dialog>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h5>QUẢN LÝ BẢN TIN</h5>
                </div>
                <div className = "row">
                    <div className = "col-5">
                        <RadioGroup row aria-label="sort"  className={classes.allSelect}
                            name="sort1" value={selectedValue} onChange={(event) =>handleSelectAll(event)} onClick = {()=> setSelectedValue("")}>
                            <FormControlLabel value="0" control={<Radio color= "primary"/>} label="Tất cả" />
                         </RadioGroup>
                        <TextField
                            className={classes.textField}
                            id="outlined-secondary"
                            label="Tìm kiếm theo tiêu đề"
                            variant="outlined"
                            color="primary"
                            size="small"
                            value = {postTitle}
                            onChange={(event) => inputChangedSearchHandler(event)}
                        />
                    </div>
                    <div className = "col-4">
                        <lable   lable className = {classes.lableField}>Ngày đăng</lable>
                        <input type="date" 
                            value = {postDate}
                            className={clsx("form-control",classes.textFieldDate)} 
                            onChange={(event) => handleSelectPostingDate(event)}  
                            max = {dateTimeFormat(now(),"dd/mmm/yyyy")}
                            placeholder ="Tìm theo ngày khởi hành" 
                        />
                    </div>
                    <div className = "col-3">
                        <Link to = {{pathname: `/admin/news/add`, oldPost: props.data}}>
                            <Button variant="contained" color="primary" >
                                Thêm tin mới
                            </Button>
                        </Link>
                    </div>
                </div>
                <Table className={classes.table} aria-label="custom pagination table">
                    <StyledTableHead>
                        <TableRow>
                            <StyledTableCell
                                style={{ width: 40 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                STT
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 230 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Tiêu đề
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 100 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Ngày đăng
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 100 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Hình ảnh
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 100 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Chi tiết
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 100 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Chỉnh sửa
                            </StyledTableCell>
                        </TableRow>
                    </StyledTableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                        ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rows).map((row) => (
                        <StyledTableRow key={row.id}>
                            <StyledTableCell style={{ width: 40 }} align="left">
                                {row.id}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 230 }} align="left">
                                {row.title}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                {dateTimeFormat(row.createdAt, "dd/mmm/yyyy")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                <img src = {row.image} alt = " " width = "100px" height ="80px"/>
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                <Link to={{pathname: `/admin/post/${row.id}`, page : page}} >
                                        <option>Xem chi tiết</option>
                                </Link>
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                <Link to={{pathname: `/admin/news/edit/${row.id}`, post : row,page : page}}>
                                    <BorderColorIcon className = {classes.IconButton} >
                                    </BorderColorIcon>
                                </Link>
                                <Link>
                                    <DeleteIcon className = {classes.IconButton}
                                        onClick={() => {
                                            handleDeleteNews(row.id)
                                        }}>
                                    </DeleteIcon>  
                                </Link>
                            </StyledTableCell> 
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <TableRow>
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: "All", value: rows.length }]}
                                colSpan={3}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { "aria-label": "rows per page" },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        data: state.getNews.data,
        error: state.getNews.error,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetNews: () => dispatch(actions.getNews()),
        onDeleteNews : (newsId) => dispatch(actions.deleteNews(newsId)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostManagement);
