import React, { useEffect} from "react";
import { makeStyles} from "@material-ui/core/styles";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Layout from "../../../hoc/Layout/Layout";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import { Link } from "react-router-dom";
import { TextField } from "@material-ui/core";
import clsx from 'clsx';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {Button} from "@material-ui/core"; 
import Spinner from 'react-spinner-material';


const useStyles2 = makeStyles({
    table: {
        marginLeft: 50,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 230,
        width: 900,
        overflow: 'hidden',
        textAlign: 'justify'
    },
    textField: {
        width: 667,
    },
    title: {
        fontFamily: "Arial !important",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 17,
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14,
    },
    positiveResult: {
        color: "green",
        fontWeight: "bold",
        fontSize: 14,
    },
    textFieldDate: {
        width: 170,
        marginLeft: 10,
        marginBottom: 10,
    },
    lableField : {
        fontSize: 15,
        marginLeft: 5,
        marginTop: 10,
        float: 'left'
    },
    dataRow : {
        marginLeft: 30,
        marginBottom: 14,
        marginRight: 30
    },
    content: {
        textAlign: 'justify'
    },
    backIcon : {
        marginLeft: 130,
    },
    backButton :{
        fontSize: 14,
        marginLeft: 40,
        float: 'left'
    },
    refreshButton : {
        marginRight: 20,
    },
    buttonSubmit: {
        marginLeft: 370
    },
    errorMessage: {
        color: "red"
    },
});

function AddPost(props) {
    let [news, setPost] = React.useState({});
    let [title, setTitle] = React.useState("");
    let [content, setContent] = React.useState("");
    let [image, setImage] = React.useState("");
    let [titleError, setTitleError] = React.useState("");
    let [contentError, setContentError] = React.useState("");
    let [previewImage, setPreviewImage] = React.useState("");
    let [selectedFile, setSelectedFile] = React.useState("");
    let [errorFormat, setErrorFormatImage] = React.useState("");
    let [errorSize, setErrorSize] = React.useState("");
    let [posts, setPosts] = React.useState([]);
    let [loading, setLoading] = React.useState(false);
    let [oldPost, setOldPost] = React.useState([]);

    useEffect(() =>{
        setOldPost(props.location.oldPost.sort((a, b) => (a.id > b.id? 1 : -1)));
        props.onGetNews();
        if(props.posts){
            setPosts(props.posts);
        }
    },[props.posts]);

    const classes = useStyles2();

    const handleChangeTitle = (event) => {
        var value = event.target.value;
        if(value !== ""){
            setTitle(value);
            setTitleError("")
        }else {
            setTitleError("Vui lòng nhập tiêu đề !");
        }
    }

    const handleChangeContent = (event, editor) => {
        var value = editor.getData();
        if(value !== ""){
            setContent(value);
            setContentError("")
        }else {
            setContentError("Vui lòng nhập nội dung bài viết !");
        }
    }

    const handleChangeImage = (event) => {
        setErrorFormatImage("");
        setErrorSize("");
        const fileTypes = ['image/png', 'image/jpeg'];
        
        var file = event.target.files;
        if(checkImage(file[0])){
            console.log(file[0]);
            let errMessage = [];
            setSelectedFile(file[0]);
            setPreviewImage(URL.createObjectURL(file[0]))
        }
    }

    const checkImage = (image) => {
        if(image.size >  4194304){
            setErrorSize("Kích thước file tải lên không cho phép !");
            return false;
        }
        if(image.type !== "image/jpeg" && image.type !== "image/png"){
            setErrorFormatImage("Định dạng file không cho phép !");
            return false;
        }
        return true;
    }

    const handleSubmitAddNews = () => {
        console.log(oldPost);
        console.log(oldPost[oldPost.length-1].id);
        if(title!== "" && content !== ""){
            setLoading(true);
            news.title = title;
            news.content = content;
            props.onAddNews(news);
            if(props.isAddSuccess){
                setTitle("");
                setImage("");
                setContent("");
            }
        }
        if(title === ""){
            setTitleError("Vui lòng nhập tiêu đề !");
        }
        if(content === ""){
            setContentError("Vui lòng nhập nội dung bài viết !");
        }
    }

    const handleResetNews = () => {
        setTitle("");
        setContent("");
    }

    const uploadHandler = () => {
        console.log(oldPost);
        console.log(oldPost[oldPost.length-1].id);
        const postId = posts!==[] ? posts[posts.length-1].id : 0;
        console.log(postId);
        const formData = new FormData();
        formData.append("newsImg", selectedFile);
        if(postId !== oldPost[oldPost.length-1].id) {
            props.onUpoadImage(postId, formData);
        }
    }

    if(props.isAddSuccess === true){
        setTimeout(() => {
            uploadHandler();
        },
        1000);
    }

    if(props.isAddSuccess && props.isUploadSuccess){
        window.location = "/admin/post-manage";
    }
     
    return (
        <Layout>
            <div>
                <Spinner size={120} spinnerColor={"#333"} spinnerWidth={2} visible={loading} />
            </div>
            <div>
                <Link  className = {classes.backButton} to={{pathname: `/admin/post-manage`}} title = "Quay lại trang trước">
                    <ArrowBackIosIcon className = {classes.backIcon}/> 
                </Link>
            </div>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h3>Thông tin chi tiết bài đăng</h3>
                </div>

                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2")}>
                        
                    </div>
                    <div className = {clsx("col-10")}>
                        <p className = {classes.errorMessage}>{titleError}</p>
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = "col-2">
                        <span><b>Tiêu đề : </b></span>
                    </div>
                    <div className = "col-10">
                        <TextField
                            className={classes.textField}
                            id="outlined-secondary"
                            label="Nhập tiêu đề"
                            variant="outlined"
                            color="primary"
                            size="small"
                            onChange={(event) => handleChangeTitle(event)}
                            defaultValue = {title}
                        />
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2")}>
                    </div>
                    <div className = {clsx("col-10")}>
                        <p className = {classes.errorMessage}>{errorSize}</p>
                        <p className = {classes.errorMessage}>{errorFormat}</p>
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = "col-2">
                        <span><b>Hình ảnh : </b></span>
                    </div>
                    <div className = "col-10">
                        <input
                            type="file"
                            accept="image/*"
                            onChange = {(event) => handleChangeImage(event)}
                            multiple
                        /><br/>
                        <p><i>*Chỉ được tải file ảnh (.png, .jpg)</i></p>
                        {previewImage !== "" ? <img src = {previewImage} alt = " " width = "300px" height ="240px"/>
                        :""}
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2")}>
                    </div>
                    <div className = {clsx("col-10")}>
                        <p className = {classes.errorMessage}>{contentError}</p>
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2", classes.content)}>
                        <p><b>Nội dung : </b></p>
                    </div>
                    <div className = {clsx("col-10", classes.textArea)}>
                        <CKEditor
                            // config={{
                            //     ckfinder: {
                            //         headers: { 'Authorization': `bearer ${localStorage.getItem('token')}`,
                            //         'X-CSRF-TOKEN': 'CSFR-Token', },
                            //         uploadUrl: 'https://doancnpm.azurewebsites.net/Image/News',
                            //         withCredentials: true,
                            //     },
                            // }}
                            editor={ ClassicEditor }
                            data= {content}
                            onInit={ editor => {
                                console.log( 'Editor is ready to use!', editor );
                            } }
                            onChange={ ( event, editor ) => handleChangeContent(event, editor) }
                            onBlur={ ( event, editor ) => {
                                console.log( 'Blur.', editor );
                            } }
                            onFocus={ ( event, editor ) => {
                                console.log( 'Focus.', editor );
                            } }
                        />
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow, classes.buttonSubmit)}>
                    <div className = "md-4">
                        <Button variant="contained" color="primary" className = {classes.refreshButton}
                            onClick = {() => handleResetNews()}
                        >
                            Reset
                        </Button>
                        <Button variant="contained" color="primary" onClick = {() => handleSubmitAddNews()}>
                            Save 
                        </Button>
                    </div>
                </div>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        data: state.addNews.data,
        posts: state.getNews.data,
        isGetNewsSuccess: state.getNews.isSuccess,
        error: state.addNews.error,
        isAddSuccess: state.addNews.isSuccess,
        isUploadSuccess: state.uploadImage.isSuccess
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onAddNews: (news) => dispatch(actions.addNews(news)),
        onGetNews: () => dispatch(actions.getNews()),
        onUpoadImage: (newsId, formData) => dispatch(actions.uploadImage(newsId, formData)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);
