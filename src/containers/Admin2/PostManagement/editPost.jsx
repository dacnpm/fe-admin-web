import React, { useEffect,useState} from "react";
import { makeStyles} from "@material-ui/core/styles";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Layout from "../../../hoc/Layout/Layout";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import clsx from 'clsx';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { CKEditor } from '@ckeditor/ckeditor5-react/dist/ckeditor';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic/build/ckeditor';
import {Button} from "@material-ui/core"; 


const useStyles2 = makeStyles({
    table: {
        marginLeft: 50,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 230,
        width: 900,
        overflow: 'hidden',
        textAlign: 'justify'
    },
    textField: {
        width: 667,
        height: 40,
    },
    title: {
        fontFamily: "Arial !important",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 17,
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14,
    },
    positiveResult: {
        color: "green",
        fontWeight: "bold",
        fontSize: 14,
    },
    textFieldDate: {
        width: 170,
        marginLeft: 10,
        marginBottom: 10,
    },
    lableField : {
        fontSize: 15,
        marginLeft: 5,
        marginTop: 10,
        float: 'left'
    },
    dataRow : {
        marginLeft: 30,
        marginBottom: 20,
        marginRight: 30
    },
    content: {
        textAlign: 'justify'
    },
    backIcon : {
        marginLeft: 130,
    },
    backButton :{
        fontSize: 14,
        marginLeft: 40,
        float: 'left'
    },
    refreshButton : {
        marginRight: 20,
    },
    buttonSubmit: {
        marginLeft: 370
    },
    errorMessage: {
        color: "red"
    },
});

function EditPost(props) {
    const { postId } = useParams();
    let [post,setPost] = React.useState({});
    let [news, setNews] = React.useState({});
    let [title, setTitle] = React.useState(props.location.post.title);
    let [content, setContent] = React.useState(props.location.post.content);
    let [image, setImage] = React.useState(props.location.post.image);
    let [titleError, setTitleError] = React.useState("");
    let [contentError, setContentError] = React.useState("");
    let [isChangeTitle, setIsChangeTitle] = React.useState(false);
    let [isChangeContent, setIsChangeContent] = React.useState(false);
    let [selectedFile, setSelectedFile] = React.useState("");
    let [previewImage, setPreviewImage] = React.useState("");
    let [errorFormat, setErrorFormatImage] = React.useState("");
    let [errorSize, setErrorSize] = React.useState("");
    let oldPage = props.location.page;

    const classes = useStyles2();

    const handleChangeTitle = (event) => {
        setIsChangeTitle(true);
        var value = event.target.value;
        if(value !== ""){
            setTitle(value);
            setTitleError("")
        }else {
            setTitleError("Vui lòng nhập tiêu đề !");
        }
    }
    const handleChangeContent = (event, editor) => {
        setIsChangeContent(true);
        var value = editor.getData();
        if(value !== ""){
            setContent(value);
            setContentError("")
        }else {
            setContentError("Vui lòng nhập nội dung bài viết !");
        }
    }
    const handleChangeImage = (event) => {
        setErrorFormatImage("");
        setErrorSize("");
        
        var file = event.target.files;
        if(checkImage(file[0])){
            console.log(file[0]);
            setSelectedFile(file[0]);
            setPreviewImage(URL.createObjectURL(file[0]))
        }
    }

    const checkImage = (image) => {
        if(image.size > 4194304){
            setErrorSize("Kích thước file tải lên không cho phép !");
            return false;
        }
        if(image.type !== "image/jpeg" && image.type !== "image/png"){
            setErrorFormatImage("Định dạng file không cho phép !");
            return false;
        }
        return true;
    }

    const handleSubmitEditNews = () => {
        if(title !== "" && content !== ""){
            news.title = title;
            //news.image = image;
            news.content = content;
            news.id = postId;
            props.onEditNews(news);
            uploadHandler();

        }
        if(title === "" && isChangeTitle === true){
            setTitleError("Vui lòng nhập tiêu đề !");
        }
        if(content === "" && isChangeContent === true){
            setContentError("Vui lòng nhập nội dung bài viết !");
        }
    }

    const handleResetNews = () => {
        setTitle(props.location.post.title);
        setContent(props.location.post.content);
        setPreviewImage(props.location.post.image);
    }

     const uploadHandler = () => {
        if(selectedFile !== ""){
        //console.log(selectedFile);
        let formData = new FormData();
        formData.append("newsImg", selectedFile);
        props.onUpoadImage(postId, formData);
        }
    }

    if(props.isSuccess && selectedFile === ""){
        window.location = "/admin/post-manage";
    }

    if(props.isUploadSuccess && props.isSuccess){
        window.location = "/admin/post-manage";
    }
    
    return (
        <Layout>
            <div>
                <Link  className = {classes.backButton} to={{pathname: `/admin/post-manage`, oldPage: oldPage}} title = "Quay lại trang trước">
                    <ArrowBackIosIcon className = {classes.backIcon}/> 
                </Link>
            </div>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h3>Chỉnh sửa bài đăng</h3>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2")}>
                    </div>
                    <div className = {clsx("col-10")}>
                        <p className = {classes.errorMessage}>{titleError}</p>
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = "col-2">
                        <span><b>Tiêu đề : </b></span>
                    </div>
                    <div className = "col-10">
                        <input
                            autoFocus
                            defaultValue = {title}
                            className={classes.textField}
                            id="outlined-secondary"
                            variant="outlined"
                            color="primary"
                            size="small"
                            onChange={(event) => handleChangeTitle(event)}
                        />
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2")}>
                    </div>
                    <div className = {clsx("col-10")}>
                        <p className = {classes.errorMessage}>{errorSize}</p>
                        <p className = {classes.errorMessage}>{errorFormat}</p>
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = "col-2">
                        <span><b>Hình ảnh : </b></span>
                    </div>
                    <div className = "col-10">
                        <input
                            type="file"
                            accept="image/*"
                            onChange = {(event) => handleChangeImage(event)}
                            multiple
                        />
                        <br/>
                        <p><i>*Chỉ được tải file ảnh (.png, .jpg)</i></p>
                        <img src = {previewImage === "" ? image+"?a="+Math.random() : previewImage } alt = " " width = "300px" height ="240px"/>
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2")}>
                    </div>
                    <div className = {clsx("col-10")}>
                        <p className = {classes.errorMessage}>{contentError}</p>
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2", classes.content)}>
                        <p><b>Nội dung : </b></p>
                    </div>
                    <div className = {clsx("col-10", classes.textArea)}>
                        <CKEditor
                            // config={{
                            //     ckfinder: {
                            //         // Upload the images to the server using the CKFinder QuickUpload command.
                            //         uploadUrl:  '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
                            //         headers: { 'Authorization': `bearer ${localStorage.getItem('token')}` }
                            //     }
                            // }}
                            editor={ ClassicEditor }
                            data = {content}
                            onReady={ editor => {
                                console.log( 'Editor is ready to use!', editor );
                            } }
                            onChange={ ( event, editor ) => handleChangeContent(event, editor) }
                            onBlur={ ( event, editor ) => {
                                console.log( 'Blur.', editor );
                            } }
                            onFocus={ ( event, editor ) => {
                                console.log( 'Focus.', editor );
                            } }
                        />
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow, classes.buttonSubmit)}>
                    <div className = "md-4">
                        <Button variant="contained" color="primary" className = {classes.refreshButton}
                             onClick = {() => handleResetNews()}
                        >
                            Reset
                        </Button>
                        <Button variant="contained" color="primary" 
                             onClick = {() => handleSubmitEditNews()}
                        >
                            Save 
                        </Button>
                    </div>
                </div>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        news: state.getNewsDetail.data,
        error: state.getNewsDetail.error,
        isGetNewsSuccess: state.getNewsDetail.isSuccess,
        isSuccess: state.updateNews.isSuccess,
        data: state.updateNews.data,
        isUploadSuccess: state.uploadImage.isSuccess
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetNewsDetail: (postId) => dispatch(actions.getNewsDetail(postId)),
        onEditNews: (news) => dispatch(actions.updateNews(news)),
        onUpoadImage: (newsId, formData) => dispatch(actions.uploadImage(newsId, formData)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPost);
