import React, { useEffect} from "react";
import { makeStyles} from "@material-ui/core/styles";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Layout from "../../../hoc/Layout/Layout";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { dateTimeFormat } from "../../../store/utility";
import clsx from 'clsx';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

const useStyles2 = makeStyles({
    table: {
        marginLeft: 50,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 230,
        width: 900,
        overflow: 'hidden',
        textAlign: 'justify'
    },
    textField: {
        width: 240,
        marginLeft: 130,
        marginBottom: 10,
        float: "left"
    },
    title: {
        fontFamily: "Arial !important",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 17,
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14,
    },
    positiveResult: {
        color: "green",
        fontWeight: "bold",
        fontSize: 14,
    },
    textFieldDate: {
        width: 170,
        marginLeft: 10,
        marginBottom: 10,
    },
    lableField : {
        fontSize: 15,
        marginLeft: 5,
        marginTop: 10,
        float: 'left'
    },
    dataRow : {
        marginLeft: 30,
        marginBottom: 20,
        marginRight: 30
    },
    content: {
        textAlign: 'justify'
    },
    backIcon : {
        marginLeft: 130,
    },
    backButton :{
        fontSize: 14,
        marginLeft: 40,
        float: 'left'
    },
});

function PostDetail(props) {
    let [post, setPost] = React.useState({});
    let [title, setTitle] = React.useState("");
    const { postId } = useParams();
    let oldPage = props.location.page;

    useEffect(() => {
        props.onGetNewsDetail(postId);
        if (props.isSuccess) {
            setPost(props.data);
            setTitle(props.data.title);
        }
    }, [props.data]);

    const classes = useStyles2();
    
    return (
        <Layout>
            <div>
                <Link  className = {classes.backButton} to={{pathname: `/admin/post-manage`,oldPage: oldPage}} title = "Quay lại trang trước">
                    <ArrowBackIosIcon className = {classes.backIcon}/> 
                </Link>
            </div>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h3>Thông tin chi tiết bài đăng</h3>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = "col-2">
                        <span><b>Tiêu đề : </b></span>
                    </div>
                    <div className = "col-10">
                        {title.toUpperCase()}
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = "col-2">
                        <span><b>Ngày đăng :</b> </span>
                    </div>
                    <div className = "col-10">
                        {dateTimeFormat(post.createdAt, "dd/mmm/yyyy")}
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = "col-2">
                        <span><b>Hình ảnh : </b></span>
                    </div>
                    <div className = "col-10">
                        <img src = {post.image} alt = " " width = "300px" height ="240px"/>
                    </div>
                </div>
                <div className = {clsx("row", classes.dataRow)}>
                    <div className = {clsx("col-2", classes.content)}>
                        <p><b>Nội dung : </b></p>
                    </div>
                    <div className = "col-10">
                        { <div dangerouslySetInnerHTML={{ __html: post.content}} /> }
                        
                    </div>
                </div>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        data: state.getNewsDetail.data,
        error: state.getNewsDetail.error,
        isSuccess: state.getNewsDetail.isSuccess
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetNewsDetail: (postId) => dispatch(actions.getNewsDetail(postId)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostDetail);
