import Layout from '../../../hoc/Layout/Layout'
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import {Box} from "@material-ui/core";
import React from 'react';
import classes from './statisticItinerary.module.css';
import {useState} from 'react';
import FormControl from '@material-ui/core/FormControl';
import moment from 'moment';
import '../../../../node_modules/react-datetime/css/react-datetime.css';
import ItineraryChart from '../../../components/Chart/ItineraryChart';
import clsx from 'clsx';

function  StatisticItinerary(props) {
    let [statisticResult, setStatisticResult]  = useState({});
    let [startDate, setStartDate] = useState();
    let [minEndDate, setminEndDate] = useState();
    let [endDate, setEndDate] = useState();

    let [display, setDisplay] = useState("none");

    const handleDateStartChange = (event) => {
        var value = event.target.value;
        if(endDate !=null){
            props.onGetStatisticItinerary(value, endDate);
        }
        setStartDate(value);
        setminEndDate(value);
    }

    const handleDateEndChange = (event) => {
        var value = event.target.value;
        setEndDate(value);
        props.onGetStatisticItinerary(startDate, value);
    }
    const submitHandler = (event) => {
        if(props.data){
            setStatisticResult(props.data.data);
            console.log(props.data.data);
            setDisplay("block");
        }
    }
      
    var content = '';
    if(statisticResult !== {}){
        content =   <div className="statisticResult">
                        <h6>Biểu đồ thống kê lượt khách đến các thành phố</h6>
                        <ItineraryChart result = {statisticResult}/>
                    </div>
    }else {
        content = "Không có lịch trình nào!";
    }
    
    return (
        <div>
            <Layout>
                <div className={classes.mainContain}>
                    <div className = {classes.title}>
                        <h1 className={classes.titleText}>Thống kê lịch trình</h1>
                    </div>
                    <div className = {classes.formCode}>
                        <FormControl className={classes.container}>
                            <div className="row">
                                <div className ="form-group col">
                                    <lable className="form-lable">Từ ngày:</lable>
                                    <input type="date" 
                                        className={clsx("form-control",classes.inputTime)} placeholder="Chọn ngày bắt đầu"  
                                        onChange={(event) =>handleDateStartChange(event)}  
                                        max = {moment().format("YYYY-MM-DD")}
                                    />
                                </div>
                                <div className ="form-group col">
                                    <lable><span>Đến ngày:</span></lable>
                                    <input type="date" 
                                        className={clsx("form-control",classes.inputTime)} placeholder="Chọn ngày kết thúc" 
                                        onChange={(event) => handleDateEndChange(event)}  
                                        min={moment(minEndDate).format("YYYY-MM-DD")}
                                    />
                                </div>
                                <div className ={clsx("form-group col",classes.submitButton)}>
                                    <button className = " btn btn-primary btn-sm" onClick = {(event)=>submitHandler(event)}>OK</button>
                                </div>
                            </div>
                        </FormControl>
                    </div>
                    <div className = {classes.confirmData}>
                        <Box maxWidth="xm" className={classes.chartContainer} display = {display}>
                            {content}
                        </Box>
                    </div>
                </div>
            </Layout>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        data: state.statisticItinerary.data,
        error: state.statisticItinerary.error
    };
};
  
  const mapDispatchToProps = dispatch => {
    return {
        onGetStatisticItinerary : (startDate, endDate) => dispatch(actions.statisticItinerary(startDate, endDate)),
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(StatisticItinerary);