import React, { useEffect,useState} from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Layout from "../../../hoc/Layout/Layout";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import Select from "@material-ui/core/Select";
import { Link } from "react-router-dom";
import { dateTimeFormat } from "../../../store/utility";
import { TextField } from "@material-ui/core";
import TableHead from "@material-ui/core/TableHead";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TablePaginationActions from '../../../components/Pagination/TablePagination';

const StyledTablePagination = withStyles((theme) => ({
    root: {
        "& .MuiTablePagination-toolbar": {
             position: "absolute !important",
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 50,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 180,
        width: 1100,
        overflow: 'hidden'
    },
    textField: {
        width: 240,
        marginLeft: 130,
        marginBottom: 10,
        float: "left"
    },
    title: {
        marginTop: 10,
        fontFamily: "Arial !important",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 17,
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14,
    },
    positiveResult: {
        color: "green",
        fontWeight: "bold",
        fontSize: 14,
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const StyledTableHead = withStyles((theme) => ({
    root: {
        "& .TableCell-head-35": {
            backgroundColor: "#132169 !important"
        },
    },
}))(TableHead);

function UserManagement(props) {
    let [rows, setData] = useState([]);
    let [searchInfo, setSearchInfo] = useState("");
    let [selectedValue, setSelectedValue] = React.useState("0");
    var users = [];
    const classes = useStyles2();
    const [page, setPage] = React.useState(props.location.oldPage != undefined ? props.location.oldPage: 0);

    useEffect(() => {
        if(!rows.length){
            props.onGetUsers();
            if (props.data) {
                setData(props.data);
            }
        }
    }, [props.data]);
    
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    users = rows;
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const inputChangedSearchHandler = (event) => {
        var value = event.target.value;
        setSearchInfo(value);
        setSelectedValue("");
    };

    const handleChange = (event) => {
        setSelectedValue(event.target.value);
    };
    const handleSelectAll = (event) => {
        setSelectedValue(event.target.value);
        setSearchInfo("");
    };
    
    if(searchInfo !== ""){
        if(selectedValue === "1"){
            rows = rows.filter(
            (row) =>(row.firstName + row.lastName)
            .toLowerCase()
            .indexOf(searchInfo.toLowerCase()) !== -1
        )
        }else if(selectedValue==="2"){
            rows = rows.filter(
            (row) => row.phoneNumber !== null && row.phoneNumber.indexOf(searchInfo) !== -1
            )
        }else if(selectedValue === "3"){
            rows = rows.filter(
            (row) => row.address !== null &&
            (row.address).toLowerCase().indexOf(searchInfo.toLowerCase()) !== -1
            )
        }else {
            rows = rows.filter(row => 
            (row.firstName + row.lastName).toLowerCase().indexOf(searchInfo.toLowerCase()) !== -1 ||
            (row.phoneNumber !== null && row.phoneNumber.indexOf(searchInfo) !== -1) ||
            (row.address !== null && (row.address).toLowerCase().indexOf(searchInfo.toLowerCase()) !== -1)
            );
        }
    }else
    {
        rows = users;
    }

    return (
        <Layout>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h5>QUẢN LÝ NGƯỜI DÙNG</h5>
                </div>
                <div className = "row">
                    <div className = "col-5">
                        <TextField
                            className={classes.textField}
                            id="outlined-secondary"
                            label="Nhập thông tin tìm kiếm"
                            variant="outlined"
                            color="primary"
                            size="small"
                            value = {searchInfo}
                            onChange={(event) => inputChangedSearchHandler(event)}
                        />
                    </div>
                    <div className = "col-7">
                        <RadioGroup label= "Tìm theo:" row aria-label="sort" name="sort1" value={selectedValue} 
                            onChange={(event) => handleChange(event)} className="col">
                            <FormControlLabel value="0" control={<Radio onChange = {(event) => handleSelectAll(event)} color= "primary"/>} label="Tất cả" />
                            <FormControlLabel value="1" control={<Radio color= "primary"/>} label="Họ tên" />
                            <FormControlLabel value="2" control={<Radio color= "primary"/>} label="SĐT" />
                            <FormControlLabel value="3" control={<Radio color= "primary"/>} label="Địa chỉ" />
                        </RadioGroup>
                    </div>
                </div>
                <Table className={classes.table} aria-label="custom pagination table">
                    <StyledTableHead>
                        <TableRow>
                            <StyledTableCell
                                style={{ width: 170 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Họ và tên
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 100 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Ngày sinh
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 120 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Giới tính
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 100 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Quốc tịch
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 100 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                SĐT
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 240 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Địa chỉ
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 170 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Tác vụ
                            </StyledTableCell>
                        </TableRow>
                    </StyledTableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                        ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rows).map((row) => (
                        <StyledTableRow key={row.id}>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                {row.lastName + " " + row.firstName}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                {dateTimeFormat(row.dateOfBirth, "dd/mmm/yyyy")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                {row.gender}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 130 }} align="left">
                                {row.nationality}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                {row.phoneNumber}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 240 }} align="left">
                                {row.address}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                <Select value={0}>
                                    <option value={0}>Xem chi tiết</option>
                                    <Link to={{pathname: `/admin/user/itinerary/${row.email}`, page : page }}>
                                        <option>Xem lịch sử lịch trình</option>
                                    </Link>
                                    <Link to={{pathname: `/admin/user/checkin/${row.accountId}`, page : page}}>
                                        <option>Xem lịch sử check in</option>
                                    </Link>
                                    <Link to={{pathname: `/admin/user/testing/${row.accountId}`, page : page}}>
                                        <option>Xem lịch sử xét nghiệm</option>
                                    </Link>
                                </Select>
                            </StyledTableCell>
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <TableRow>
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: "All", value: rows.length }]}
                                colSpan={3}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { "aria-label": "rows per page" },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        data: state.getUsers.data,
        error: state.getUsers.error,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetUsers: () => dispatch(actions.getUsers()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserManagement);
