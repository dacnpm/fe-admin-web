import React, { useEffect,useState} from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Layout from "../../../hoc/Layout/Layout";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import Select from "@material-ui/core/Select";
import { Link } from "react-router-dom";
import { dateTimeFormat } from "../../../store/utility";
import TableHead from "@material-ui/core/TableHead";
import { useParams } from "react-router-dom";
import {now} from 'moment';
import clsx from 'clsx';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import TablePaginationActions from '../../../components/Pagination/TablePagination';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const StyledTablePagination = withStyles((theme) => ({
    root: {
        "& .MuiTablePagination-toolbar": {
             position: "absolute !important",
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 50,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 180,
        width: 1100,
        overflow: 'hidden'
    },
    textField: {
        width: 170,
        marginLeft: 70,
        marginBottom: 10,
    },
    title: {
        fontFamily: "Arial !important",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 17,
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14,
    },
    positiveResult: {
        color: "green",
        fontWeight: "bold",
        fontSize: 14,
    },
    backButton : {
        float: "left",
        marginLeft: 40
    },
    backIcon : {
        float: "left"
    },
    selectedField:{
        float: "left",
        marginRight: 20
    },
    lableField:{
        fontSize: 16,
        marginTop: 7,
        float: "left",
        marginRight: 3
    },
    allSelect : {
        marginLeft: 40,
        marginRight: 14,
        float: 'left'
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function UserItinerary(props) {
    let [departureCityName, setDepartureCity] = useState("");
    let [destinationCityName, setDestinationCity] = useState("");
    let [departureTime, setDepartureTime] = useState("");
    let [itineraries, setUserItinerary] = useState([]);
    let [cities, setCityList] = useState([]);
    let [selectedValue, setSelectedValue] = React.useState("0");
    var oldItineraries = [];
    const { email } = useParams();
    let oldPage = props.location.page;

    useEffect(() => {
        if(!cities.length && !itineraries.length)
        {
            props.onGetCityList();
            props.onGetUserItinerary(email);
            if (props.data) {
                setUserItinerary(props.data);
            }
            if(props.cityList){
                setCityList(props.cityList);
            }
        }
    }, [email, props.cityList]);

    oldItineraries = itineraries ;

    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const inputChangedSearchHandler = (event) => {
        var value = event.target.value;
        setDepartureTime(value);
        setSelectedValue("");
    };

    const handleSelectDepartureCity = (event) => {
        var value = event.target.value;
        setDepartureCity(value);
        setSelectedValue("");
    }

    const handleSelectDestinationCity = (event) => {
        var value = event.target.value;
        setDestinationCity(value);
        setSelectedValue("");
    }

    const handleSelectAll = (event) => {
        setSelectedValue(event.target.value);
        setDepartureCity("");
        setDestinationCity("");
        setDepartureTime("");
    };

    if(departureCityName !== "" ){
        itineraries = itineraries.filter(
            (row) => row.departure == departureCityName)
    }
    if(destinationCityName !== "" ){
        itineraries = itineraries.filter(
            (row) => row.destination == destinationCityName)
    }
    if(departureTime !== ""){
        itineraries = itineraries.filter(
            (row) => dateTimeFormat(row.departureTime, "dd/mmm/yyyy") === dateTimeFormat(departureTime, "dd/mmm/yyyy")
        );
    }
    if(selectedValue === "0"){
        itineraries = oldItineraries;
    }

    return (
        <Layout>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h5>DANH SÁCH LỊCH TRÌNH</h5>
                </div>
                <Link  className = {classes.backButton} to={{pathname: `/admin/user-manage`, oldPage: oldPage}}>
                    <ArrowBackIosIcon className = {classes.backIcon}/> 
                    Quay lại
                </Link>
                <RadioGroup row aria-label="sort"  className={classes.allSelect}
                    name="sort1" value={selectedValue} onChange={(event) =>handleSelectAll(event)} onClick = {()=> setSelectedValue("")}>
                    <FormControlLabel value="0" control={<Radio color= "primary"/>} label="Tất cả" />
                </RadioGroup>
                <Select
                    className = {classes.selectedField}
                    native
                    onChange={(event) => handleSelectDepartureCity(event)}
                    value = {departureCityName}
                    >
                    <option value= "">Chọn Thành phố khởi hành</option>
                    {cities.map(city =>  (
                        <option key= {city.id} value = {city.name} 
                        
                        >{city.name}</option>
                    ) 
                    )}
                </Select>
                <Select
                    className = {classes.selectedField}
                    native
                    onChange={(event) => handleSelectDestinationCity(event)}
                    value = {destinationCityName}
                    >
                    <option value= "">Chọn Thành phố đến</option>
                    {cities.map(city =>  (
                        <option key= {city.id} value = {city.name} 
                        
                        >{city.name}</option>
                    ) 
                    )}
                </Select>
                <lable className = {classes.lableField}>Ngày khởi hành</lable>
                <input type="date" 
                    className={clsx("form-control",classes.textField)} title="Tìm theo ngày khởi hành"  
                    onChange={(event) => inputChangedSearchHandler(event)}  
                    max = {dateTimeFormat(now(),"dd/mmm/yyyy")}
                    value = {departureTime}
                    placeholder ="Tìm theo ngày khởi hành" 
                />
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell
                                style={{ width: 270 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                               Địa điểm khởi hành
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 300 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Địa điểm đến
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 270 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Thời gian khởi hành
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 330 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Thời gian đến
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 300 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Số hiệu phương tiện
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                        ? itineraries.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : itineraries).map((row) => (
                        <StyledTableRow key={row.id}>
                            <StyledTableCell style={{ width: 300 }} align="left">
                               {row.departure}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 270 }} align="left">
                               {row.destination}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 330 }} align="left">
                                {dateTimeFormat(row.departureTime,"ddd  dd/mmm/yyyy HH:MM:ss TT")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 300 }} align="left">
                                {dateTimeFormat(row.landingTime,"ddd  dd/mmm/yyyy HH:MM:ss TT")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 300 }} align="left">
                               {row.travelNo}
                            </StyledTableCell>
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <TableRow>
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: "All", value: itineraries.length }]}
                                colSpan={3}
                                count={itineraries.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { "aria-label": "rows per page" },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        data: state.getUserItinerary.data,
        error: state.getUserItinerary.error,
        cityList: state.getCityList.data
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetUserItinerary: (email) => dispatch(actions.getUserItinerary(email)),
        onGetCityList: () => dispatch(actions.getCity()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserItinerary);
