import React, { useEffect,useState} from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Layout from "../../../hoc/Layout/Layout";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import { Link } from "react-router-dom";
import { dateTimeFormat } from "../../../store/utility";
import TableHead from "@material-ui/core/TableHead";
import { useParams } from "react-router-dom";
import {now} from 'moment';
import clsx from 'clsx';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import TablePaginationActions from '../../../components/Pagination/TablePagination';

const StyledTablePagination = withStyles((theme) => ({
    root: {
        "& .MuiTablePagination-toolbar": {
             position: "absolute !important",
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 50,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 180,
        width: 1100,
        overflow: 'hidden'
    },
    textField: {
        width: 240,
        marginLeft: 430,
        marginBottom: 10,
    },
    title: {
        marginTop: 10,
        fontFamily: "Arial !important",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 17,
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14,
    },
    positiveResult: {
        color: "green",
        fontWeight: "bold",
        fontSize: 14,
    },
    backButton : {
        float: "left",
        marginLeft: 40
    },
    backIcon : {
        float: "left"
    }
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function UserCheckinLocation(props) {
    let [checkinTime, setCheckinTime] = useState("");
    let [userCheckin, setUserCheckin] = useState([]);
    const { accountId } = useParams();
    let oldPage = props.location.page;

    useEffect(() => {
        if(!userCheckin.length)
        {
            props.onGetUserCheckin(accountId);
            if (props.data) {
                setUserCheckin(props.data);
            }
        }
    }, [props.data]);

    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const inputChangedSearchHandler = (event) => {
        var value = event.target.value;
        setCheckinTime(value);
    };

    if(checkinTime !== ""){
        userCheckin = userCheckin.filter(
            (row) => dateTimeFormat(row.time, "dd/mmm/yyyy") === dateTimeFormat(checkinTime, "dd/mmm/yyyy")
        );
    }

    return (
        <Layout>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h5>DANH SÁCH ĐỊA ĐIỂM CHECKIN</h5>
                </div>
                <Link  className = {classes.backButton} to={{pathname: `/admin/user-manage`, oldPage: oldPage}}>
                    <ArrowBackIosIcon className = {classes.backIcon}/> 
                    Quay lại
                </Link>
                <input type="date" 
                    className={clsx("form-control",classes.textField)} placeholder="Tìm theo ngày check in"  
                    onChange={(event) => inputChangedSearchHandler(event)}  
                    max = {dateTimeFormat(now(),"dd/mmm/yyyy")}
                />
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell
                                style={{ width: 270 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                               Thời gian
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 300 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Địa điểm
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                        ? userCheckin.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : userCheckin).map((row) => (
                        <StyledTableRow key={row.id}>
                            <StyledTableCell style={{ width: 200 }} align="left">
                                {dateTimeFormat(row.time,"ddd  dd/mmm/yyyy HH:MM:ss TT")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 300 }} align="left">
                               {row.address}
                            </StyledTableCell>
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <TableRow>
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: "All", value: userCheckin.length }]}
                                colSpan={3}
                                count={userCheckin.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { "aria-label": "rows per page" },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        data: state.getUserCheckin.data,
        error: state.getUserCheckin.error,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetUserCheckin: (accountId) => dispatch(actions.getUserCheckin(accountId)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserCheckinLocation);
