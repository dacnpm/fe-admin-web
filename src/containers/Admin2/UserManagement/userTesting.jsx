import React, { useEffect,useState} from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Layout from "../../../hoc/Layout/Layout";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import { Link } from "react-router-dom";
import { dateTimeFormat } from "../../../store/utility";
import { TextField } from "@material-ui/core";
import TableHead from "@material-ui/core/TableHead";
import { useParams } from "react-router-dom";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import TablePaginationActions from '../../../components/Pagination/TablePagination';

const StyledTablePagination = withStyles((theme) => ({
    root: {
        "& .MuiTablePagination-toolbar": {
             position: "absolute !important",
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 50,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 180,
        width: 1100,
        overflow: 'hidden'
    },
    textField: {
        width: 240,
        marginLeft: 430,
        marginBottom: 10,
    },
    title: {
        marginTop: 10,
        fontFamily: "Arial !important",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 17,
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14,
    },
    positiveResult: {
        color: "green",
        fontWeight: "bold",
        fontSize: 14,
    },
    backButton : {
        float: "left",
        marginLeft: 40
    },
    backIcon : {
        float: "left"
    }
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function UserTesting(props) {
    let [testingLocationName, setTestingLocationName] = useState("");
    let [userTestings, setUserTestings] = useState([]);
    const { accountId } = useParams();
    let oldPage = props.location.page;

    useEffect(() => {
        if(!userTestings.length)
        {
            props.onGetUserTesting(accountId);
            if (props.data) {
                setUserTestings(props.data);
            }
        }
    }, [props.data]);

    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const inputChangedSearchHandler = (event) => {
        var value = event.target.value;
        setTestingLocationName(value);
    };

    userTestings = userTestings.filter(
        (row) =>(row.testingLocation.name)
        .toLowerCase()
        .indexOf(testingLocationName.toLowerCase()) !== -1
    );

    return (
        <Layout>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h5>DANH SÁCH LỊCH SỬ XÉT NGHIỆM</h5>
                </div>
                <Link  className = {classes.backButton} to={{pathname: `/admin/user-manage`, oldPage: oldPage}}>
                    <ArrowBackIosIcon className = {classes.backIcon}/> 
                    Quay lại
                </Link>
                <TextField
                    className={classes.textField}
                    id="outlined-secondary"
                    label="Tìm theo nơi xét nghiệm"
                    variant="outlined"
                    color="primary"
                    size="small"
                    onChange={(event) => inputChangedSearchHandler(event)}
                />
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell
                                style={{ width: 170 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Ngày xét nghiệm
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 170 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Tên bệnh viện
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 120 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Địa chỉ
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 100 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Phí XN
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 130 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Kết quả
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                        ? userTestings.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : userTestings).map((row) => (
                        <StyledTableRow key={row.id}>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                {dateTimeFormat(row.testingDate,"ddd  dd/mmm/yyyy HH:MM:ss TT")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                               {row.testingLocation.name}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                {row.testingLocation.address}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 100 }} align="left">
                                {row.cost}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 130 }} align="left">
                                <p className = {row.result === "Negative"? classes.negativeResult : classes.positiveResult}>
                                    {row.result === "Negative"? "Dương tính" : (row.result === "Positive" ? "Âm tính" : "Chưa có kết quả")}
                                </p>
                            </StyledTableCell>
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <TableRow>
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: "All", value: userTestings.length }]}
                                colSpan={3}
                                count={userTestings.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { "aria-label": "rows per page" },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        data: state.getUserTesting.data,
        error: state.getUserTesting.error,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetUserTesting: (accountId) => dispatch(actions.getUserTesting(accountId)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserTesting);
