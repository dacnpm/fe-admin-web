import React, {useEffect,useState} from 'react';
import { makeStyles, withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Layout from './../../../../hoc/Layout/Layout';
import ConfirmIsPaidButton from '../../../../components/UI/Button/ConfirmPaidButton';
import { connect } from 'react-redux';
import * as actions from '../../../../store/actions/index';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {dateTimeFormat} from '../../../../store/utility';
import TableHead from "@material-ui/core/TableHead";
import TablePaginationActions from '../../../../components/Pagination/TablePagination';
import { TextField} from '@material-ui/core';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const StyledTablePagination = withStyles((theme) => ({
    root: {
        '& .MuiTablePagination-toolbar': {
            position: 'absolute !important'
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 48,
        maxWidth: 1000,
    },
    paper: {
        marginLeft: 140,
        width: 1100,
        overflow: 'hidden'
    },
    title: {
        marginBottom: 10,
        fontFamily: "Arial !important",
        fontSize: 17,
        textAlign: "center",
    },
    thumbnail: {
        fontWeight: "bold"
    },
    tableFooter : {
        marginBottom: 200,
        marginLeft: 100
    },
    negativeResult: {
        color: "red",
        fontWeight: "bold",
        fontSize: 14
    },
    positiveResult : {
        color: "green",
        fontWeight: "bold",
        fontSize: 14
    },
    locationSelect : {
        marginTop: 7,
        float: 'left',
        width: 240,
        marginRight: 10,
    },
    citySelect : {
        width: 120,
        marginRight: 10,
        marginTop: 7,
        float: 'left'
    },
    testingTextField: {
        marginBottom: 10,
        float: 'left',
        marginRight: 10
    },
    isPaidSelect : {
        marginTop: 7,
        marginLeft: 50,
        marginRight: 10,
        float: 'left'
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function SatisticalResult(props) {
    let [rows, setData] = useState([]);
    let [locations, setLocation] = useState([]);
    let [locationId, setLocationId] = useState(0);
    let [cityId, setCityId] = useState(0);
    let [fullName, setFullName] = React.useState("");
    let [selectedValue, setSelectedValue] = React.useState("0");
    var testings = [];
    let [cities, setCities] = useState([]);
    
    useEffect(() =>{
        props.onGetTesting();
        props.onGetLocation();
        props.onGetCityList();
        if(props.data){
            setData(props.data);
        }
        if(props.locations){
            setLocation(props.locations);
        }
        if(props.cities){
            setCities(props.cities);
        }

    },[props.isGetLocationSuccess,props.data]);

    rows = rows.filter(row => row.testingStateId !== 0);
    testings = rows;
    testings = testings.sort((a, b) => (a.id < b.id? 1 : -1));
    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeResult = (event,testing) => {
        const result = event.target.value;
        var updateData ={
            id: testing.id,
            testingState : 2,
            result: result,
            isPaid: testing.isPaid
        }
        actions.updateTesting(updateData.id, updateData);
    };

    const handleSelectIsPaid = (event) => {
        setSelectedValue(event.target.value);
    };

    const handleChangeLocation = (event) => {
        var value = event.target.value;
        setLocationId(value);
        setSelectedValue("");
    }
    
    const handleChangeCity = (event) => {
        var value = event.target.value;
        setCityId(value);
        setSelectedValue("");
    }

    const inputChangedNameHandler = (event) => {
        var value = event.target.value;
        setFullName(value);
        setSelectedValue("");
    }

    const handleSelectAll = (event) => {
        setSelectedValue(event.target.value);
        setCityId(0);
        setLocationId(0);
        setFullName("");
    };

    if(rows !== []){
        rows = rows.sort((a, b) => (a.id < b.id? 1 : -1));
    }
    if(cityId !== 0){
        locations = locations.filter(location => location.cityId == cityId);
    }

    if(locationId !== 0){
        rows = rows.filter(test => test.testingLocation.id == locationId);
    }
    if(fullName!==""){
        rows = rows.filter(test => test.fullName.toLowerCase().indexOf(fullName.toLowerCase()) !== -1);
    }
    if(selectedValue !== "0"){
        if(selectedValue === "1"){
            rows = rows.filter(test => test.isPaid == true);
        }else if(selectedValue === "2"){
            rows = rows.filter(test => test.isPaid == false);
        }
    }else {
        rows = testings;
    }

  return (
        <Layout>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h3>Quản lý xét nghiệm</h3>
                </div>
                <RadioGroup row aria-label="sort"  className={classes.isPaidSelect}
                 name="sort1" value={selectedValue} onChange={(event) =>handleSelectIsPaid(event)} onClick = {()=> setSelectedValue("")}>
                    <FormControlLabel value="0" control={<Radio color= "primary" onChange = {(event) => handleSelectAll(event)}/>} label="Tất cả" />
                    <FormControlLabel value="1" control={<Radio color= "primary"/>} label="Đã trả phí" />
                    <FormControlLabel value="2" control={<Radio color= "primary"/>} label="Chưa trả phí" />
                </RadioGroup>
                <Select
                    native
                    onChange={(event) => handleChangeCity(event)}
                    value = {cityId}
                    className={classes.citySelect}
                    >
                    <option value= {0} disabled>Thành phố</option>
                    {cities.map(city =>  (
                        <option key= {city.id} value = {city.id}
                        >{city.name}</option>
                    ) 
                    )}
                </Select>
                <Select
                    native
                    onChange={(event) => handleChangeLocation(event)}
                    value = {locationId}
                    className={classes.locationSelect}
                    >
                    <option value= {0} disabled>Chọn địa điểm</option>
                    {locations.map(location =>  (
                        <option key= {location.id} value = {location.id}
                        >{location.name}</option>
                    ) 
                    )}
                </Select>
                <TextField
                    id="outlined-secondary"
                    label="Nhập tên tìm kiếm"
                    variant="outlined"
                    size = "small"
                    color="primary"
                    value = {fullName}
                    className={classes.testingTextField}
                    onChange={(event) => inputChangedNameHandler(event)}
                />
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell style={{ width: 90 }} align="left" className={classes.thumbnail}>
                                Mã XN
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left" className={classes.thumbnail}>
                                Họ và tên
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170}} align="left" className={classes.thumbnail}>
                                Địa điểm XN
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left" className={classes.thumbnail}>
                                Thời gian XN
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left" className={classes.thumbnail}>
                                Phí xét nghiệm
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 190 }} align="left" className={classes.thumbnail}>
                                Xác nhận trả phí
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left" className={classes.thumbnail}>
                                Kết quả
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                        ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : rows).map((row) => ( 
                        <StyledTableRow key={row.id}>
                            <StyledTableCell style={{ width: 90 }} align="left">
                                {row.id}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                {row.fullName}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                {row.testingLocation.name}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                {dateTimeFormat(row.testingDate, "ddd  dd/mmm/yyyy HH:MM:ss TT")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                {row.cost} VNĐ
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 190 }} align="left">
                                <ConfirmIsPaidButton isPaid = {row.isPaid} testing = {row}/>

    
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 170 }} align="left">
                                <p className = {row.result === "Negative"? classes.negativeResult : classes.positiveResult}>
                                    {row.result === "Negative"? "Âm tính" : (row.result === "Positive" ? "Dương tính" : "Chưa có kết quả")}
                                </p>
                                <FormControl>
                                    <Select
                                        disabled = {row.isPaid === true ? "": "disabled"}
                                        native
                                        onChange={(event, testing) => handleChangeResult(event, row)}
                                        >
                                        <option value="">Cập nhật</option>
                                        <option value={false}>Âm tính</option>
                                        <option value={true}>Dương tính</option>
                                    </Select>
                                </FormControl>
                            </StyledTableCell>
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <TableRow>
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: rows.length }]}
                                colSpan={3}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { 'aria-label': 'rows per page' },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = state => {
    return {
        data: state.getTesting.data,
        error: state.getTesting.error,
        locations: state.getLocation.data,
        isGetTestingSuccess: state.getTesting.isSuccess,
        isGetLocationSuccess: state.getLocation.isSuccess,
        cities : state.getCityList.data,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetTesting: () => dispatch(actions.getTesting()),
        onGetLocation : () => dispatch(actions.getLocation()),
        onGetCityList: () => dispatch(actions.getCity()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SatisticalResult);
