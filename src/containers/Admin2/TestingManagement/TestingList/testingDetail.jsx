import { connect } from 'react-redux';
import * as actions from '../../../../store/actions/index';
import {Grid, Paper,Typography, Box} from "@material-ui/core";
import Layout from './../../../../hoc/Layout/Layout';
import React from 'react'
import classes from './detail.module.css';
import {useEffect, useState} from 'react';
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import {dateTimeFormat} from '../../../../store/utility';


function TestingDetail(props) {
    let [testingDetail, setTestingDetail] = useState({});
    const { testingId } = useParams();
    let [locationName, setLocationName] = useState("");
    let [locationAddress, setLocationAddress] = useState("");
    let oldCity = props.location.cityId;
    let oldLocationId = props.location.locationId;

    useEffect(() =>{
        props.onGetTestingDetail(testingId);
        if(props.testing){
            setTestingDetail(props.testing);
            setLocationName(props.testing.testingLocation.name);
            setLocationAddress(props.testing.testingLocation.address);
        }

    }, [props.testing]);

       
    return (
        <div>
            <Layout>
                <div className={classes.mainContain}>
                    </div>
                    <Link  className = {classes.backButton} to={{pathname: `/admin/list-testing`, oldCity: oldCity, oldLocation: oldLocationId}}>
                        <ArrowBackIosIcon className = {classes.backIcon}/> 
                        <p>Về trang trước</p>
                    </Link>
                    <div className = {classes.confirmData}>
                        <Box maxWidth="xm" className={classes.container}>
                            <Typography component="div" style={{ backgroundColor: '#fff', height: '65vh' }}>
                                <Paper className = {classes.infoTitle}>
                                    THÔNG TIN XÉT NGHIỆM CHI TIẾT
                                </Paper>
                                <div className={classes.information}>
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <table>
                                                <tr>
                                                    <th>Ngày đăng ký xét nghiệm:</th>
                                                    <td>{dateTimeFormat(testingDetail.registerDate,  "ddd  dd/mmm/yyyy HH:MM:ss TT")}</td>
                                                </tr>
                                                <tr>
                                                    <th>Ngày xét nghiệm:</th>
                                                    <td>{dateTimeFormat(testingDetail.testingDate,  "ddd  dd/mmm/yyyy HH:MM:ss TT")}</td>
                                                </tr>
                                                <tr>
                                                    <th>Nơi xét nghiệm: </th>
                                                    <td>{locationName }</td>
                                                </tr>
                                                <tr>
                                                    <th>Địa chỉ</th>
                                                    <td>{locationAddress}</td>
                                                </tr>
                                                <tr>
                                                    <th>Phí xét nghiệm:</th>
                                                    <td>{testingDetail.cost} VNĐ</td>
                                                </tr>
                                                <tr>
                                                    <th>Tình trạng: </th>
                                                    <td>{testingDetail.isPaid === false ? "Chưa thanh toán":"Đã thanh toán"}</td>
                                                </tr>
                                                <tr>
                                                    <th>Kết quả: </th>
                                                    <td>{testingDetail.result === "Negative" ? "Âm tính" : "Dương tính"}</td>
                                                </tr>
                                            </table>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Typography>
                        </Box>
                    </div>
            </Layout>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        isSuccess: state.getTestingDetail.isSuccess,
        testing: state.getTestingDetail.data
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      onGetTestingDetail: (testingId) => dispatch(actions.getTestingDetail(testingId)),
      onCloseModalError: () => dispatch(actions.closeModalErrorgetTestingDetail)
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(TestingDetail);
