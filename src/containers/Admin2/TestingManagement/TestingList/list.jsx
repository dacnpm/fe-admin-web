import Layout from './../../../../hoc/Layout/Layout';
import { connect } from 'react-redux';
import * as actions from '../../../../store/actions/index';
import {withStyles } from "@material-ui/core/styles";
import { TextField} from '@material-ui/core';
import {Box} from "@material-ui/core";
import React from 'react'
import classes from './list.module.css';
import {useEffect,useState} from 'react';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import TestingResultChart from '../../../../components/Chart/PieChart';
import {dateTimeFormat} from '../../../../store/utility';
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TablePaginationActions from '../../../../components/Pagination/TablePagination';
import moment from 'moment';

function  TestingList(props) {
    let [testing, setTesting] = useState([]);
    let [locationId, setLocationId] = useState(props.location.oldLocation != undefined ?props.location.oldLocation : 3);
    let [locations, setLocation] = useState([]);
    let [statisticResult, setResult] = useState({});
    let [selectedValue, setSelectedValue] = React.useState("0");
    let [fullName, setFullName] = React.useState("");
    let [cityList, setCityList] = React.useState([]);
    let [selectedCity, setSelectCity] = React.useState(props.location.oldCity != undefined ?props.location.oldCity : 6)
    let [isChangeSelect, setIsChangeSelect] = useState(false);

    const StyledTablePagination = withStyles((theme) => ({
        root: {
            "& .MuiTablePagination-toolbar": {
                position: "absolute !important",
            },
        },
    }))(TablePagination);

    const StyledTableRow = withStyles((theme) => ({
        root: {
            "&:nth-of-type(odd)": {
                backgroundColor: theme.palette.action.hover,
            },
        },
    }))(TableRow);

    useEffect(() =>{
        props.onGetTesting();
        props.onGetLocation();
        props.onGetCityList();
        props.onGetStatisticTesting(locationId);
        if(props.statisticedResult){
            setResult(props.statisticedResult);
        }
        if(props.testing){
          setTesting(props.testing);

        }
        if(props.locations){
            setLocation(props.locations);
        }
        if(props.cityList){
            setCityList(props.cityList);
        }
      },[props.isGetCitySuccess, props.isGetLocationSuccess]);

      console.log(props);
    const handleChangeLocation = (event) => {
        var value = event.target.value;
        setLocationId(value);
        props.onGetStatisticTesting(value);
        if(props.statisticedResult){
            setResult(props.statisticedResult);
        }
        setIsChangeSelect(true);
    }

    const inputChangedNameHandler = (event) => {
        setInterval(() => {
            var value = event.target.value;
            setFullName(value);
        }, 2000);
    }

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(3);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChange = (event) => {
        setSelectedValue(event.target.value);
    };

    const handleSelectCity = (event) => {
        var value = event.target.value;
        setSelectCity(value);
        setIsChangeSelect(true);
    }

    if(selectedCity !== 0){
        locations = locations.filter(row => row.cityId == selectedCity);
    }else {
        locations = locations;
    }

    var content = '' ;
    var testingChart = '';

    testing = testing.filter(test => test.testingLocation.id == locationId);
    if(props.isSuccess){
        console.log(statisticResult)
        if(fullName!==""){
            testing = testing.filter(test => test.fullName.toLowerCase().indexOf(fullName.toLowerCase()) !== -1);
        }
        if(selectedValue === "0"){
            testing = testing.filter(test => test.testingLocation.id == locationId);
        }else if(selectedValue === "1"){
            testing = testing.filter(test => test.result == "Negative");
        }else if(selectedValue === "2") {
            testing = testing.filter(test => test.result == "Positive");
        }else if(selectedValue === "3" ){
            testing = testing.filter(test => test.result == "Chưa có kết quả");
        }
        
        var totalTesting = props.statisticedResult.positiveCases + props.statisticedResult.negativeCases + props.statisticedResult.pendingCases;
        var positiveCases = Math.round(props.statisticedResult.positiveCases/totalTesting * 100);
        var negativeCases = Math.round(props.statisticedResult.negativeCases/totalTesting * 100);
        var pendingCases = Math.round(props.statisticedResult.pendingCases/totalTesting * 100);
        console.log(totalTesting, positiveCases, negativeCases, pendingCases);
        
        content =   <div className="statisticResult">
                        <table className="table-primary table-striped">
                            <tr className = {classes.tableHead}>
                                <th>Tổng XN</th>
                                <th>Dương tính</th>
                                <th>Âm tính</th>
                                <th>Đang xử lý</th>
                            </tr>
                            <tr  className="table-secondary">
                                <td  className={classes.txt}>{props.statisticedResult.positiveCases + props.statisticedResult.negativeCases + props.statisticedResult.pendingCases}</td>
                                <td className={classes.txt}>{props.statisticedResult.negativeCases}</td>
                                <td className={classes.txt}>{props.statisticedResult.positiveCases}</td>
                                <td className={classes.txt}>{props.statisticedResult.pendingCases}</td>
                            </tr>
                        </table>
                    </div>
        testingChart = <TestingResultChart className={classes.chartBox}
                            totalTesting = {totalTesting}
                            positivePercent = {positiveCases}
                            negativePercent = {negativeCases}
                            pendingPercent = {pendingCases}
                        />
    }
    return (
        <div>
            <Layout>
                <div className={classes.mainContain}>
                    <div className = {classes.title}>
                        <h1 className={classes.titleText}>Thống kê xét nghiệm</h1>
                    </div>
                    <div className = {classes.formCode}>
                        <FormControl>
                            <div>
                            <Select
                                className = {classes.selectedField}
                                native
                                onChange={(event) => handleSelectCity(event)}
                                value = {selectedCity}>
                                {cityList.map(city =>  (
                                    <option key= {city.id} value = {city.id} 
                                    >{city.name}</option>
                                ) 
                                )}
                            </Select>
                                <Select
                                    native
                                    onChange={(event) => handleChangeLocation(event)}
                                    value = {locationId}
                                    >
                                    <option value="" >Chọn địa điểm</option>
                                    {locations.map(location =>  (
                                        <option key= {location.id} value = {location.id}
                                        >{location.name}</option>
                                    ) 
                                    )}
                                </Select>
                            </div>
                        </FormControl>
                    </div>
                    <div className = {clsx("row",classes.confirmData)}>
                        <div className = {clsx("col-3",classes.chart)} >
                            <Box >
                                {testingChart}
                            </Box>
                        </div>
                        <div className = "col-9">
                            <Box maxWidth="xm" className={classes.container} >
                                {content}
                                <div className ={clsx("row",classes.testingList)}> 
                                    <TextField
                                        id="outlined-secondary"
                                        label="Nhập tên tìm kiếm"
                                        variant="outlined"
                                        size = "small"
                                        color="primary"
                                        className={clsx("col-2.5",classes.testingTextField)}
                                        onChange={(event) => inputChangedNameHandler(event)}
                                    />
                                    <RadioGroup row aria-label="sort" name="sort1" value={selectedValue} onChange={(event) => handleChange(event)} className="col">
                                        <FormControlLabel value="0" control={<Radio color= "primary"/>} label="Tất cả" />
                                        <FormControlLabel value="1" control={<Radio color= "primary"/>} label="Dương tính" />
                                        <FormControlLabel value="2" control={<Radio color= "primary"/>} label="Âm tính" />
                                        <FormControlLabel value="3" control={<Radio color= "primary"/>} label="Đang chờ" />
                                    </RadioGroup>
                                </div>
                            </Box>
                            <Box maxWidth="xm" className={classes.testingTable}>
                                <table className="table table-secondary">
                                    <tr>
                                        <th>Họ và tên</th>
                                        <th>Ngày ĐK</th>
                                        <th>Ngày XN</th>
                                        <th>Xem chi tiết</th>
                                    </tr>
                                    {(rowsPerPage > 0
                                    ? testing.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    : testing).map((item) => (
                                        <tr key ={item.id}>
                                            <td>{item.fullName}</td>
                                            <td>{dateTimeFormat(moment(item.registerDate).parseZone(),  "ddd  dd/mmm/yyyy HH:MM:ss TT")}</td>
                                            <td>{dateTimeFormat(item.testingDate,  "ddd  dd/mmm/yyyy HH:MM:ss TT")}</td>
                                            <td>
                                                <Link to = {{
                                                    pathname: `/admin/testing/${item.id}`,
                                                    cityId: selectedCity,
                                                    locationId: locationId
                                                }}>
                                                    <button className = "btn btn-sm btn-primary" >Xem chi tiết</button>
                                                </Link>
                                            </td>
                                        </tr>
                                    ))}
                            <TableFooter style={{ width: 700 }} className="">
                            <StyledTableRow>
                                <StyledTablePagination
                                    rowsPerPageOptions={[3, 10, 25, { label: "All", value: testing.length }]}
                                    colSpan={3}
                                    count={testing.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    SelectProps={{
                                    inputProps: { "aria-label": "rows per page" },
                                    native: true,
                                    }}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                    ActionsComponent={TablePaginationActions}
                                    labelRowsPerPage = "Số hàng hiển thị: "
                                />
                            </StyledTableRow>
                            </TableFooter>
                                </table>
                            </Box>
                        </div>
                    </div>
                </div>
            </Layout>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        testing: state.getTesting.data,
        isGetTestingSuccess: state.getTesting.isSuccess,
        locations: state.getLocation.data,
        isGetLocationSuccess: state.getLocation.isSuccess,
        testingError: state.getTesting.error,
        locationError: state.getLocation.error,
        statisticedResult: state.statisticTesting.data,
        isSuccess: state.statisticTesting.isSuccess,
        cityList: state.getCityList.data,
        isGetCitySuccess: state.getCityList.isSuccess
    };
  };
  
const mapDispatchToProps = dispatch => {
    return {
        onGetTesting: () => dispatch(actions.getTesting()),
        onGetLocation : () => dispatch(actions.getLocation()),
        onGetStatisticTesting : (locationId) => dispatch(actions.statisticTesting(locationId)),
        onGetCityList : () => dispatch(actions.getCity()),
    };
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(TestingList);