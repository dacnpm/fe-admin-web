import React, { useEffect,useState} from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Layout from "./../../../../hoc/Layout/Layout";
import ConfirmGroupButton from "../../../../components/UI/Button/ConfirmGroupButton";
import { connect } from "react-redux";
import * as actions from "../../../../store/actions/index";
import { dateTimeFormat } from "../../../../store/utility";
import TableHead from "@material-ui/core/TableHead";
import TablePaginationActions from '../../../../components/Pagination/TablePagination';
import Select from '@material-ui/core/Select';
import { TextField} from '@material-ui/core';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {now} from 'moment';
import clsx from 'clsx';
import moment from 'moment';


const StyledTablePagination = withStyles((theme) => ({
    root: {
        "& .MuiTablePagination-toolbar": {
            position: "absolute !important",
        },
    },
}))(TablePagination);

const useStyles2 = makeStyles({
    table: {
        marginLeft: 34,
        maxWidth: 1140,
    },
    paper: {
        marginLeft: 92,
        width: 1200,
        overflow: 'hidden'
    },
    title: {
        marginBottom: 10,
        fontFamily: "Arial !important",
        fontSize: 17,
        textAlign: "center",
    },
    thumbnail: {
        fontWeight: "bold",
    },
    tableFooter: {
        marginBottom: 200,
        marginLeft: 100,
    },
    locationSelect : {
        marginTop: 7,
        marginRight: 20,
        float: 'left'
    },
    citySelect : {
        width: 120,
        marginRight: 10,
        marginTop: 7,
        float: 'left'
    },
    testingTextField: {
        marginBottom: 10,
        float: 'left',
        marginRight: 30
    },
    textField: {
        width: 170,
        marginLeft: 10,
        marginBottom: 10,
    },
    lableField : {
        fontSize: 15,
        marginLeft: 5,
        marginTop: 10,
        float: 'left'
    },
    allSelect : {
        marginLeft: 60,
        marginRight: 10,
        marginTop: 7,
        float: 'left'
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#132169",
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function ConfirmTesting(props) {
    let [rows, setData] = useState([]);
    let [locationId, setLocationId] = useState(0);
    let [cityId, setCityId] = useState(0);
    let [locations, setLocation] = useState([]);
    let [fullName, setFullName] = React.useState("");
    let [registerDate, setRegisterDate] = React.useState("");
    let [selectedValue, setSelectedValue] = React.useState("0");
    let [cities, setCities] = useState([]);
    var testings = [];

    useEffect(() => {
        props.onGetTesting();
        props.onGetLocation();
        props.onGetCityList();
        if (props.data) {
            setData(props.data);
        }
        if(props.locations){
            setLocation(props.locations);
        }
        if(props.cities){
            setCities(props.cities);
        }
    }, [props.isGetLocationSuccess, props.data]);

    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeLocation = (event) => {
        var value = event.target.value;
        setLocationId(value);
        setSelectedValue("");
    }

    const handleChangeCity = (event) => {
        var value = event.target.value;
        setSelectedValue("");
        setCityId(value);
    }

    const inputChangedNameHandler = (event) => {
        var value = event.target.value;
        setFullName(value);
        setSelectedValue("");
    }
    
    const handleSelectRegisterDate = (event) => {
        setRegisterDate(event.target.value);
        setSelectedValue("");
    };

    const handleSelectAll = (event) => {
        setSelectedValue(event.target.value);
        setCityId(0);
        setLocationId(0);
        setFullName("");
        setRegisterDate("");
    };

    rows = rows.filter(row => row.testingStateId === 0);
    testings = rows;

    if(cityId !== 0){
        locations = locations.filter(location => location.cityId == cityId);
    }
    if(locationId !== 0){
        rows = rows.filter(test => test.testingLocation.id == locationId);
    }
    if(fullName!==""){
        rows = rows.filter(test => test.fullName.toLowerCase().indexOf(fullName.toLowerCase()) !== -1);
    }
    if(registerDate !== ""){
        rows = rows.filter(
            (row) => dateTimeFormat(row.registerDate, "dd/mmm/yyyy") === dateTimeFormat(registerDate, "dd/mmm/yyyy")
        );
    }
    if(selectedValue === "0"){
        rows =testings;
    }

    return (
        <Layout>
            <TableContainer component={Paper} className={classes.paper}>
                <div className={classes.title}>
                    <h3>Danh sách đăng ký xét nghiệm</h3>
                </div>
                <RadioGroup row aria-label="sort"  className={classes.allSelect}
                 name="sort1" value={selectedValue} onChange={(event) =>handleSelectAll(event)} onClick = {()=> setSelectedValue("")}>
                    <FormControlLabel value="0" control={<Radio color= "primary"/>} label="Tất cả" />
                </RadioGroup>
                <Select
                    native
                    onChange={(event) => handleChangeCity(event)}
                    value = {cityId}
                    className={classes.citySelect}
                    >
                    <option value= {0} disabled>Thành phố</option>
                    {cities.map(city =>  (
                        <option key= {city.id} value = {city.id}
                        >{city.name}</option>
                    ) 
                    )}
                </Select>
                <Select
                    native
                    onChange={(event) => handleChangeLocation(event)}
                    value = {locationId}
                    className={classes.locationSelect}
                    >
                    <option value="0" >Chọn địa điểm xét nghiệm</option>
                    {locations.map(location =>  (
                        <option key= {location.id} value = {location.id}
                        >{location.name}</option>
                    ) 
                    )}
                </Select>
                <TextField
                    id="outlined-secondary"
                    label="Nhập tên tìm kiếm"
                    variant="outlined"
                    size = "small"
                    color="primary"
                    value = {fullName}
                    className={classes.testingTextField}
                    onChange={(event) => inputChangedNameHandler(event)}
                />
                <lable className = {classes.lableField}>Ngày đăng ký</lable>
                <input type="date" 
                    className={clsx("form-control",classes.textField)} 
                    onChange={(event) => handleSelectRegisterDate(event)}  
                    max = {dateTimeFormat(now(),"dd/mmm/yyyy")}
                    value = {registerDate}
                    placeholder ="Tìm theo ngày khởi hành" 
                />
                
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell
                            style={{ width: 70 }}
                            align="left"
                            className={classes.thumbnail}
                            >
                                Mã XN
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 140 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Họ và tên
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 260 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Địa điểm xét nghiệm
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 200 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Thời gian đăng ký
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 200 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Thời gian xét nghiệm
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ width: 140 }}
                                align="left"
                                className={classes.thumbnail}
                            >
                                Tình trạng
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {(rowsPerPage > 0
                    ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    : rows).map((row) => (
                        <StyledTableRow key={row.id}>
                            <StyledTableCell style={{ width: 70 }} align="left">
                                {row.id}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 140 }} align="left">
                                {row.fullName}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 260 }} align="left">
                                {row.testingLocation.name}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 200 }} align="left">
                                {dateTimeFormat(moment(row.registerDate).parseZone(),"GMT:ddd  dd/mmm/yyyy HH:MM:ss TT")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 200 }} align="left">
                            {dateTimeFormat(row.testingDate,"ddd  dd/mmm/yyyy HH:MM:ss TT")}
                            </StyledTableCell>
                            <StyledTableCell style={{ width: 140 }} align="left">
                                <ConfirmGroupButton
                                    status={row.testingStateId}
                                    testing={row}
                                />
                            </StyledTableCell>
                        </StyledTableRow>
                        ))}
                    </TableBody>
                    <TableFooter style={{ width: 700 }} className="">
                        <StyledTableRow>
                            <StyledTablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: "All", value: rows.length }]}
                                colSpan={3}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                inputProps: { "aria-label": "rows per page" },
                                native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                                labelRowsPerPage = "Số hàng hiển thị: "
                            />
                        </StyledTableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Layout>
    );
}

const mapStateToProps = (state) => {
    return {
        data: state.getTesting.data,
        error: state.getTesting.error,
        locations: state.getLocation.data,
        isGetLocationSuccess: state.getLocation.isSuccess,
        cities : state.getCityList.data,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGetTesting: () => dispatch(actions.getTesting()),
        onGetLocation : () => dispatch(actions.getLocation()),
        onGetCityList: () => dispatch(actions.getCity()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmTesting);
