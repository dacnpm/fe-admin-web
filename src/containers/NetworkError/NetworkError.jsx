import React  from 'react';
import classes from './NetworkError.module.css';

const NetworkError = () => {
    return ( 
        <div className={classes.mainContain}>
            <h2>Network Error!</h2>
            <p>Có lỗi xảy ra do server hoặc đường truyền mạng. Vui lòng thử lại!</p>
        </div>
    );
}
 
export default NetworkError;