import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import loginReducer from './store/reducers/login';
import confirmDataReducer from './store/reducers/confirmData';
import getTestingReducer from './store/reducers/getTesting';
import updateTestingReducer from './store/reducers/updateTestingState';
import getLocationReducer from './store/reducers/getLocation';
import statisticTestingReducer from './store/reducers/statisticTesting';
import getTestingDetailReducer from './store/reducers/getTestingDetail'
import statisticItineraryReducer from './store/reducers/statisticItinerary';
import getCityListReducer from './store/reducers/getCityList';
import getUsersReducer from './store/reducers/getUserList';
import getUserTestingReducer from './store/reducers/getUserTesting';
import getUserCheckinReducer from './store/reducers/getUserCheckin';
import getUserItineraryReducer from './store/reducers/getUserItinerary';
import getNewsReducer from './store/reducers/getNews';
import getNewsDetailReducer from './store/reducers/getNewsDetail';
import addNewsReducer from './store/reducers/addNews';
import updateNewsReducer from './store/reducers/updateNews';
import deleteNewsReducer from './store/reducers/deleteNews';
import addCityReducer from './store/reducers/addCity';
import updateCityReducer from './store/reducers/updateCity'; 
import addTestingLocationReducer from './store/reducers/addTestingLocation';
import updateTestingLocationReducer from './store/reducers/updateTestingLocation';
import uploadImageReducer from './store/reducers/uploadImage';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    login: loginReducer,
    confirm: confirmDataReducer,
    getTesting: getTestingReducer,
    updateTesting: updateTestingReducer,
    getLocation: getLocationReducer,
    updateLocation: updateTestingLocationReducer,
    addLocation: addTestingLocationReducer,
    statisticTesting: statisticTestingReducer,
    getTestingDetail: getTestingDetailReducer,
    statisticItinerary: statisticItineraryReducer,
    getCityList : getCityListReducer,
    addCity: addCityReducer,
    updateCity: updateCityReducer,
    getUsers: getUsersReducer,
    getUserTesting: getUserTestingReducer,
    getUserCheckin: getUserCheckinReducer,
    getUserItinerary: getUserItineraryReducer,
    getNews: getNewsReducer,
    getNewsDetail: getNewsDetailReducer,
    addNews: addNewsReducer,
    updateNews: updateNewsReducer,
    deleteNews: deleteNewsReducer,
    uploadImage: uploadImageReducer,
});

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

export default store;

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
