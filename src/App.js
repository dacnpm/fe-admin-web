import React from "react";
import Auxx from '../src/hoc/Auxx/Auxx';
import './App.css';
import {Route ,Switch, Redirect} from 'react-router-dom';
import DashBoard from './containers/Dashboard/dashboard.jsx'
import NotFound from './containers/NotFound/notFound';
import ConfirmData from './containers/Admin1/ConfirmData/confirm';
import SatisticalUser from './containers/Admin1/SatisticalUser/satistical';
import PostManagement from './containers/Admin2/PostManagement/post_manage';
import ConfirmTesting from './containers/Admin2/TestingManagement/ConfirmTesting/confirm';
import SatisticalResult from './containers/Admin2/TestingManagement/SatisticalResult/satistical';
import TestingList from './containers/Admin2/TestingManagement/TestingList/list';
import TestingDetail from './containers/Admin2/TestingManagement/TestingList/testingDetail';
import UserManagement from './containers/Admin2/UserManagement/user_manage';
import StatisticItinerary from './containers/Admin2/Itinerary/statistic'
import Login from './containers/Login/login';
import 'bootstrap/dist/css/bootstrap.min.css';
import ListCity from './containers/Admin2/CityManagement/listCity'
import TestingLocationList from './containers/Admin2/TestingLocationManagement/locationList'
import AuthenticatedRoute from './AuthenticatedRoute';
import UserItinerary from './containers/Admin2/UserManagement/userItinerary';
import UserCheckinLocation from './containers/Admin2/UserManagement/userCheckinLocation';
import UserTesting from './containers/Admin2/UserManagement/userTesting';
import PostDetail from './containers/Admin2/PostManagement/postDetail';
import AddPost from './containers/Admin2/PostManagement/addPost';
import EditPost from './containers/Admin2/PostManagement/editPost';
import NetworkError from "./containers/NetworkError/NetworkError";

export default function App() {
    var  isAuthenticated = false; 
    var  isRoleAdmin_2 = null;
  
    if(localStorage.getItem('token') != null){
        isAuthenticated = true;
        if(localStorage.getItem('roles')==='ADMIN2'){
            isRoleAdmin_2 = true;
        } 
    }

    return (
        <Auxx>
            <Switch>
                <Route path = "/login" component ={Login} exact >
                </Route>
                <Route path = "/network-error" component ={NetworkError} exact >
                </Route>
                <AuthenticatedRoute
                    path="/" exact

                    appProps={{isAuthenticated} }
                >
                    <Redirect to = '/admin/dashboard' ></Redirect>
                </AuthenticatedRoute>
                <AuthenticatedRoute
                    path="/admin/dashboard" exact
                    component={DashBoard}
                    appProps={{isAuthenticated} }
                />
                <AuthenticatedRoute
                    path="/admin/confirm-data" exact
                    component={ConfirmData}
                    appProps={{ isAuthenticated }}
                />
                <AuthenticatedRoute
                    path="admin/satistical-user" exact
                    component={SatisticalUser}
                    appProps={{ isAuthenticated  }}
                />
                <AuthenticatedRoute
                    path="/admin/post-manage" exact
                    component={PostManagement}
                    appProps={{ isAuthenticated , isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/user-manage" exact
                    component={UserManagement}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/confirm-testing" exact
                    component={ConfirmTesting}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/satistical-testing" exact
                    component={SatisticalResult}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/list-testing" exact
                    component={TestingList}
                    appProps={{ isAuthenticated , isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/testing/:testingId" exact
                    component={TestingDetail}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/user/itinerary/:email" exact
                    component={UserItinerary}
                    appProps={{ isAuthenticated , isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/user/checkin/:accountId" exact
                    component={UserCheckinLocation}
                    appProps={{ isAuthenticated, isRoleAdmin_2 }}
                />
                <AuthenticatedRoute
                    path="/admin/user/testing/:accountId" exact
                    component={UserTesting}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/itinerary/statistic" exact
                    component={StatisticItinerary}
                    appProps={{ isAuthenticated}}
                />
                <AuthenticatedRoute
                    path="/admin/city" exact
                    component={ListCity}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/location" 
                    component={TestingLocationList}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/post/:postId" exact
                    component={PostDetail}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/news/add" exact
                    component={AddPost}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <AuthenticatedRoute
                    path="/admin/news/edit/:postId" exact
                    component={EditPost}
                    appProps={{ isAuthenticated, isRoleAdmin_2}}
                />
                <Route
                    component={NotFound}
                />
            </Switch>
        </Auxx>
    );
}
