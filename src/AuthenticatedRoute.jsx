import React from "react";
import {Route, Redirect} from 'react-router-dom';


export default function AuthenticatedRoute({ component: C, appProps, ...rest }) {
    return (
        appProps.isRoleAdmin_2 !== undefined ?
        <Route
            {...rest}
            render={props =>
            appProps.isAuthenticated && appProps.isRoleAdmin_2
            ? <C {...props} {...appProps} />
            : <Redirect
                to={`/login`}
            />}
        />
        :
        <Route
            {...rest}
            render={props =>
            appProps.isAuthenticated
            ? <C {...props} {...appProps} />
            : <Redirect
                to={`/login`}
            />}
        />
    );
  }