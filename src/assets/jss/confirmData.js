import {
    successColor,
    whiteColor,
    grayColor,
    hexToRgb
  } from "./material-dashboard-react.js";
  const confirmDataStyles = {
    successText: {
        color: successColor[0]
    },
    titleText: {
        fontSize: "40px"
    },
    title: {
        textAlign: "center"
    },
    mainContain : {
        paddingTop: "20px",
        textAlign: "center"
    },
    textField : {
        backgroundColor : "#ffffff",
        paddingBottom: "2px"
    },
    formCode : {
        marginTop : "30px"
    }
  };
  
  export default confirmDataStyles;
  